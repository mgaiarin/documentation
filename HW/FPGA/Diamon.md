# Diamon Lattice
Informazioni di base per l'utilizzo di Diamon Lattice.

## Build
Per eseguire la build di un progetto e quindi generare il bitstream bisogna:
1. Importare il progetto.
2. Andare sulla finestra `Process`.
3. Tasto destro su `bitstream` -> `Run`.

### Troubleshoot
1. Sul progetto `CloudIO` è presente il file `synth_nodes_win.txt` che definisce il nome del PC su cui si sta lavorando. Questo file va editato altrimenti la build non va a buon fine. Non so se è sempre presente nei progetti o solo in questo, da verificare.

## Programming
Dopo aver creato il bitstream è possibile programmare l'FPGA.  
Per la programmazione:
1. Andare su `Tools -> Programmer`.
2. Se si usa JTAG, fare uno scan del progetto e dei device collegati: `Design -> JTAG Scan`.
3. Selezionare il tipo di accesso e operazione da eseguire.
4. Verificare i cavi collegati con `Detect Cable`.
5. Eseguire la programmazione con `Design -> Program`.

## Export files for SPI programming
Per programmare l'FPGA da Linux tramite SPI è necessario esportare i file `<projectname>_algo.sea` e `<projectname>_data.sed`.
Questa operazione si può fare tramite il tool: `Diamon Deployment Tool` che si trova nella cartella di installazione di Lattice.
Per il deployment:
1. Avviare il tool
2. Selezionare su `Function Type`: `Embedded System`.
3. Su `Output File Type`: `SLave SPI Embedded`.
4. Selezionare il file bistream come sorgente.
5. Continuare fino allo step 4 e generare i file.