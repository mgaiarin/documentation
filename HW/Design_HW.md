## Design tips

* Resistenze di terminazione
  * Sempre da valutare il loro utilizzo
  * Solitamente una buona soluzione si ottiene impostando le corrette regole di impedenza
  * Alcuni driver invece lo richiedono e quindi serve
  * La cosa migliore e la simulazione
* Match di impedenza
  * non serve per le riflessioni, serve per limitare i disturbi visti dalla pista
  * Queando si impostano le differenziali si lavora sui gap e non sulla dimensione della pista
    * Questo perché se per ottennere l'impedenza differenziale modifico quella single ended rischio di introdurre variazioni di impedenza che si trasformano in riflessioni
  * BEST PRACTICE:
    * Impostare dimensione di linea per avere un impedenza single ended corretta
    * A questo punto si può modificare il GAP per raggiungere la differenziale corretta
      * Nota: spesso avere un impedenza diff inferiore alla nominale va bene (e.g. ETH a 90ohm è ottimo a 110 invece e uno schifo)
      * L'impedenza differenziale serve a ridurre i disturbi esterni, è più è basso meglio è
  * Accordions:
    * Meglio farlo ad ogni curva e non alla fine della linea.
      * In questo modo riduci mismatch di impedenza e tieni piste allineate.
* Via in PAD
  * È possibile mettere via in pad con via cechi
    * l via ceco evita il risucchio della pasta
    * Se lo riempi lo devi rendere piatto perché se ci va una ball sopra potresti avere problemi in assemblaggio
* Embedded capacitance
  * Si ottiene creando un layer molto piccolo di FR4 e due piani VCC e GND
  * Si genera così un piano capacitivo può sostituire le capacità di bypass
* Zone vuote
  * Se ci sono zone vuote nel PCB e bene inserire dei via per migliorare il processo di etching
* Quadrotto
  * Meglio fornire i dati e lasciare il produttore fare il lavoro per generare il quadrotto
* Mai mischiare massa e alimentazioni
* Scrivere sempre correnti e tensioni su schematico per avere sempre tutte le info sotto mano
  * Va bene una tabella vicino ad ogni regolatore
  * ripple misurato o desiderato
    * tutto questo aiuta anche il debug
* Verificare alla fine dello sbroglio sempre tutti i piani per evitare strozzature e tagli troppo grossi
* Piste:
  * mai stare sotto i 4mils
* Ferriti
  * tagliano i livelli, quindi possono essere deleteri per i segnali digitali
  * I segnali digitali lavorano sui fronti, quindi le ferriti potrebbero causare più problemi che altro
  * utili per emissioni in alta freq.
* Va bene fare delle simulazioni prima dello sbroglio
  * Questo aiuta a capire ad esempio le massime lunghezze di linea
  * puoi valutare quindi la necessità di un repeter o no
  * La cosa aiuta su segnali 10GHz

## Produzione

* Finiture
  * ENIG: Hanno costi superiori perché richiedono moltin processi per inserire i pin d'oro
    * Questo ha la capacità di rimanere in magazzino non montato senza grossi problemi (storage)
    * Questo solitamente è la soluzione più usata, però bisogna valutare sempre le alternative in base all'uso
    * L'oro galvanico non necessita obbligatoriamente dell'ENIG, si può fare anche con altro. L'oro galvanico cresce sempre sul rame
  * Stagno chimico: ottime caratteristiche ma crea difficoltà nel montaggio bottom
  * Argento chimco: Ottimo per bagnabilità e planarità.
    * Il problema è che, come l'oro, anche qui troppo argento contamina la pasta è può dare problemi di resistenza meccanica
  * HASL: Planarità peggiore ma ormai va bene anche per BGA con passi di almeno 1mm
    * Ha il vantaggio di avere costi molto ridotti
  * OSP: La più usata su consumo commerciale (producono e non mettono via)
    * Il problema vero qui è lo storage. Va mantenuto a magazzino a temp. e umidità controllate
    * Ha costi molto bassi e ottime caratteristiche

### Vantaggi svantaggi

* immersion Sn
  * Thin whiskers:
    * Filamenti di stagno che si possono creare e che possono portare a cortocircuiti
    * La cosa si può verificare dopo la stagnatura per varie cause. Il problema è quindi latente ed è difficilmente controllabile
    * Sono nati soprattuto dopo che è stato rimosso il piombo dallo stagno.
    * Si verifica nel' (stagno chimico)
* HASL:
  * Si usa pasta saldante e poi si scalda con l'aria calda
  * Questoi può portare ad accumuli sulla piazzola e i componenti potrebbero essere un po' sollevati.
* OSP
  * Bisogna stare attenti allo storage, dopo un po' potrebbe non essere più possibile fare una reflow
