# Design guide

## Stackup

Lo stackup influenza molto tutto il design, per farlo al meglio bisogna seguire le linee guida del produttore scelto e quelle interne.  
Si riportano una serie di best practice per migliorare su questa fase della progettazione.

Cosa fare:

* per prima cosa bisogna identificare l'impedenza single ended maggiore che si dovrà routare.
  * La pista a più alta impedenza sarà quella con dimensioni minori (Es. 50 ohm).
    * Parendo da questa è possibile fissare le piste più piccole che si potranno routare. ES. si vogliono routare piste con dimensione minima di 0.1mm (caso tipico CB 6 layer)
* Azioni da svolgere sul calcolatore per identificare lo stackup corretto
  * Fissare spessori e Oz per gli strati di metallo
    * Per CB 6 strati solitamente si fissano 1 Oz strati esterni e 0.5 Oz strati interni
      * Gli Oz da usare dipendono dalle correnti in gioco, questi valori in generale sono validi per l'elettronica digitale che si sviluppa in DAVE.
      * Vedere sempre anche i valori consigliati dal produttore.
  * Strati di dielettrico:
    * Questa è in generale la parte in cui prestare più attenzione:
      * Valori critici da tenere in considerazione:
        * Materiale di base: Materiale del dielettrrico, sono fibre di vetro intrecciate tra loro. Fornito dal produttore, ci sono solitamente svariati tipi di materiali
        * Glass Style: Indica il tipo di intreccio che c'é tra le fibre di vetro.
        * Dk: Constante dielettrica della fibra di vetro
        * Percentuale di resina RC%: Per incollare i vari strati serve della resina che viene aggiunta al dielettrico, meno resina è presente maggiore sarà la precisione del Dk. Il Dk varia con la frequenza!
        * Spessore: spessore dello strato di dielettrico
        * Tipo di strato: Uno strato può essere `core` o `PP`.
          * Il core viene usato su "panini" pre assemblati rame - core -rame. Il panino viene poi incollato ad altri strati tramite il PP.
          * Solitamente un 6 layer è fomato così: rame - PP - rame - core - rame - PP - rame - core - rame - PP - rame
    * Step per definire i vari strati:
      * Impostare impedenza single ended (a più alta impedenza) e dimensioni della pista volute.
      * Cercare nella tabella dei materiali forniti dal produttore uno che abbia: spessore, Dk e RC% che ci permetta di avere un impedenza uguale o superiore (meglio, entro il 10%) di quella desiderata.
      * Nel caso tipico di una CB 6 layer lo strato di PP centrale è meglio averlo con uno spessore molto grande per disaccopiare i segnali sui layer 3 e 4.
  * Una volta identificato lo stackup per l'impedenza SE più alta (nell'es. 50Ohm) é possibile definire tutte le altre impedenze che avranno dimensioni maggiori.

## Bypass Capacitor

* Utilizzati per fornire corrente quando c'è un cambio repentino di tensione, per mantenere il livello di tensione precedente.
  * In questo modo si ottengono dei transitori più smussati
  * Il livello di tensione rimane più stabile
* La scelta di mettere solitamente una 100nF
  * Questo valore è sufficientemente alto da coprire tutta la carica richiesta da un IC generico
    * Valore comodo in generale, potrebbe andare bene anche un valore inferiore
  * Questo taglio di capacità ha permette di usare package più piccoli
    * Questo è vantaggioso perchè si hanno livelli ESL e ESR minori
  * Ceramici vs Tantalio
    * Ceramici hanno livelli inferiori di ESR e ESL
      * Più sale la freq più i ceramici hanno una miglior risposta ai disturbi
    * Tantalio usati solitamente per compensare variazioni a più bassa freq. con valori di capacità più alti
* Vanno sempre posizionati vicino all;ingresso dell'IC
* Se in dubbio posizionare sempre una 100nF e una 10uF nelle alimentazioni
  * Se il problema sono solo le alte freq: usare solo la 100nF
* Se bisogna compensare una devizione di tensione più lunga (basse freq) serve una capacità maggiore (es. 47uF)

## Ferriti

* Come per le capacità di bypass servono ad filtrare i segnali di alimentazione
* Non vanno considerate delle semplici induttanze ma può essere modellato come una resistenza in serie a un L//R//C
  * Non va considerato neanche come un filtro passa basso, infatti ha un range di funzionamento definito
  * Hanno un effetto resistivo ad alte f -> L'energia viene dissipata sotto forma di calore
  * A differenza di un induttore è molto più difficile avere fenomeni di risonanza con capacità parassite
* Impedenza@f
  * L'effetto dominante è quello induttivo fino ad un certo range (solitamente 30 - 500MHz)
  * In questo punto l'effetto capacitivo diventa dominante e l'impedenza reattiva diminuisce al crescere della f
  * Mentre l'impedenza non reattiva (data dalla R) inizia a dominare all'aumentare della f.
  * Da qui si capisce la cosa importante: La ferrite dissipa l'energia ad alta frequenza ed ha un valore volutamente resistivo in questa fascia di freq
  * Inoltre la resistenza serie riduce l'effetto di risonanza LC
* Un parametro importante da verificare è la massima corrente.
  * Se si supera il valore potrebbero esserci danneggiamenti
* Essendoci un effetto resistivo bisogna considerare anche una caduta di tensione che può essere un problema a tensioni basse
* Come sfruttare al meglio la ferrite [TBD]
