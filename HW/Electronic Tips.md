# Electronic tips

## C Bypass

* Ad ogni componente è sempre bene aggiungere una capacità di bypass all'alimentazione
  * Inserire sempre, anche quando non specificato una 100n

## MOS

* Inserire una resistenza serie sul gate è sempre una buona idea
  * Si crea un circuito RC con la capacità parassita C_gs nelle transizioni -> Si riduce l'impulso di corrente
  * Scegliere il valore basandosi sulla tensione applicata al gate e banalmente con la legge di ohm.
* Inserire una resistenza verso massa è una buona pratica: valore compreso tra 1K e 1M
  * Questa soluzione permette di ridurre i fenomeni di ringing sul gate del mosfet e stabilizza la tensione evitando possibili accensioni non desiderate.
## Stackup

Following various stackup and the main characteristics 

* Stackup 6 Layer
	* Version SIG - GND - SIG - SIG - PWR - SIG
		* This is the worst from the EMI point of view. It's good only for DC circuits.
	* Version SIG - GND - PWR - SIG - GND - SIG
		* This is very good, we can split the routing in this way like in the image.![[6layer-pcb-1.png]]
## Routing

Let's see some good routing practice 
* Return path. The current return path will follow the trace on the layer that is beneath it.
	* Avoid differebt reference plane.
	* The use of stitching capacitors is also recommended if the power plane is also the reference plane for a high-speed signal that creates a return path to the current source![[Figure20a-returnpath.png]] ![[Figure20b-splitplanerouting.png]] ![[Figure20c-stitching.png]]
* If you need to place components in the middle of the line is better to avoid stubs.![[Figure21b-stubs.png]]
* Accordions must have enough room in order to avoid cross talk![[Figure21a-seperation.png]]
* Transmission lines
	* Antenna problems: The highest levels of radiation are obtained if the antenna trace length is λ/4, λ/2, or λ. However, if the length is shorter than around λ/20 of the carrier frequency, then no antenna effect is expected to be observed. As a rule of thumb, we use a figure of λ/40 for the maximum length to be on the safe side.
	* The second problem comes from the signal rise time, as it is directly related to the bandwidth. The sharper the edges, the higher the bandwidth. For a microstrip configuration on an FR4 board, the signal travels at a speed of 6.146 ps/mm. Thinking about a signal that has a rise time of 340 ps, the trace may be un-terminated if it is shorter than a length of (1/10)*(340/6.146) = 5.53mm. It is always better to have a termination resistor, but a shorter trace means there should be no issues with reflections and standing waves.

## Lines termination

Some good practice
* Reference Altium Doc [[https://resources.altium.com/p/transmission-lines-and-terminations-in-high-speed-design]]
* Locating termination resistors
	* Parallel resistors: anywhere after the signal has been delivered
	* Series termination: is intended to sum up with the output characteristic impedance of the driver, it needs to be close enough, meaning that the trace connecting the two, is short enough that it does not function as a transmission line isolating one resistance from the other. The only way to arrive at an acceptable length for the connection is to use a simulator to see how long this connection can be and still have an acceptable waveform at the receiver.
* Resistors value
	* Parallel: The value of the line
	* Series: This value should sum up with the output impedance of the driver so it required IBIS or SPICE model.