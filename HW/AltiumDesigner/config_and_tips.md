# Configurazioni Altium Designer

## PCB

### Configurazioni environment
Configurazioni generali per migliorare lo sviluppo della PCB:
1. Visualizzare la griglia: right click in empty space -> grid manager -> double click -> fine dots -> select white color.
2. Selezionare da PCB a Schematico e viceversa: Tools -> Cross Select Mode [check]
3. Evidenziare le connessioni senza premere shift: configs -> PCB editor -> Board Insight Display -> Live Highlight only when Shift Key down [uncheck]
4. Evitare che AD sposti il focus sul componente quando lo seleziono da schematico: preferences -> System -> Navigation -> Cross Select Mode -> Cross Selection [check only]
   1. Se serve lo zoom sul componente (magari per cercarlo in mezzo a molti) Dimming e Zooming [check]
5. Per piazzare un certo componente senza cercarlo: Selezionare da schematico il componente e poi Tools -> component Placement -> Reposition Selected Components
6. Evidenziare una violazione: preferences -> PCB Editor -> DRC violation Display -> Solid override color [or the violation required]
7. Assegnare i colori alle nets: PCB pannel -> select nets -> right click in the net to modify -> change Net color
8. Coprire le vias con il colore della PCB: vias preferences -> Solder Mask Expansion -> tended [check].
9. Settare il comportamento delle track: preferencess -> PCB Editor -> Interactive Routing -> Routing Conflict Resolution - [Ignore Obstacles, Push obstacles, Wolk around Obstacles]  [check] (Verifcare altre config comode nel caso)
10. **IMPORTANTE** Evitare che vengano spostate le vias: preferencess -> PCB Editor -> Interactive Routing -> Interactive Routing Options -> Alow Via pushing [uncheck]
11. Spostare solo la via senza spostare la track: preferencess -> PCB Editor -> Interactive Routing -> Dragging -> select via/trak -> select 'move' (Nota: trascinando direttamente la via verrà mossa insieme alla track comunque)
12. Modifica della visualizzazione della Single Layer Mode: preferencess -> PCB editor -> Board Insight Display -> Available Single Layer Modes -> Grey Sale or Monochrome [check]
13. Se si vogliono fare dei poligoni di connessione per i pad senza che vengano tagliati: Design -> Rules -> Plane -> Poligon Connect Style -> set 'Direct Connect'
14. Per evitare che il Pour venga fatto anche sulla stessa net: polygon preferencess -> select 'Pour Over All same Net Objects
15. Per fare un repour automatico dopo un modifica: preferencess -> PCB Editor -> General -> Poligon Rebuild -> 'Repour Polygons After Modifications' / 'Repour all dependent poligons after editing' [check]
16. Nei poligoni, per evitare che ci siano pezzi molto fini di rame tra un via e l'altro: preferences polygon -> Reomve Necks When copper Width Less Than -> Set 0.2mm
17. Setup delle connessioni dei vias (Rimozione del thermal pad che rende più semplice la saldatura, ma che normalmente non serve per le via che connettono due layer): PCB rules -> Plane -> PlaneConnect -> Via connection -> Direct connect. Inoltre settare [Conductor Width 1mm, Air Gap 0.5, Expansion 0.508]

### Stack
La gestione degli stack si trova in design -> Layer Stack Manager

I layer si dividono in:
1. Signal: Dove si tracciano i segnali (come top/bottom layer)
2. Plane: Dove ci sono i piani completi e le traccie indicano degli spazi di separazione nel piano (di default e tutto rame e le traccie tolgono rame, es. piano GND)

Di base si trovano:
1. Top overlay
2. Top solder
3. Top layer (Piano in rame)
4. Dielettrico
5. Bottom Layer (piano in rame)
6. Bottom solder
7. Bottom overlay

### Roules
Le regole si trovano in design -> roules
Le regole sono da concordare subito all'inizio dello sviluppo, anche valutando i costi della scheda. Di seguito alcuni valori tipici.
1. Electrical
   1. Distanza tra le tracks (Clearence) : 0.2
2. Routing:
   1. width
      1. width: prefered 0.2 and max 10mm
      2. **Importante** La regola non si applica sulla stessa Net. Se ad esempio ho un via vicino un track della stessa net verificare che ci sia la distanza minima voluta o si rischia di pagare di più la board (o peggio avere dei problemi durante la lavorazione).
      Volendo è possibile creare una regola ad hoc per questo caso.
   2. Routing via style:
      1. RoutingVias:
         1. Hole size: min/max/preferd: 0.3 (costi minori)
         2. Diameter: min/max/preferd: 0.6

### Shortcut
1. G permette di variare la diensione della grid.
2. L sposta il componente da top to bottom side.
3. J ti fa selezionare uno specifico punto dove piazzare il componente.
4. F5 toggle per mostrare i colori delle nets.
5. ctrl + W per inserire le tracks.
6. shift + S per visualizzare in single Layer mode.

### Tips
1. Verificare Sempre linee guida fornite dal produttore.
1. Nei connettori stare attenti alla selezione, si rischia di selezionare il pad e non il componente.
2. Creare le view TOP, BOTTOM e TOP + BOTTOM.
3. Allineare i componenti automaticamente: right click -> align -> allineamento desiderato.
4. Selezionado più componenti posso vedere tutte le connessioni assieme per vedere se la posizione e buona.
5. Selezionare i pad con ctrl + right cick per evidenziare i pad da connettere. Inoltre con le parentesi quadre si modifica il contrasto.
6. A volte possono rimanere degli errori (violations) anche dopo il fix. In questo caso eseguire nuovamente il check delle violazioni.
7. Quando si vanno a migliorare le connessioni della scheda e bene andare in PCB pannel -> Nets -> [Net to improve] e selezionare l'opzione 'Dim' per evidenziare la Net di interesse.

### Regole generali per le connesioni
1. Nella prima fase della progettazione non perdere tempo a fare buone connessioni, basta collegare tutto.
2. Inizialmente non conviene partire con i poligoni, perché sono difficili da gestire. Meglio partire con delle track in un primo momento.
3. Se un pad ha molte connesioni si può mettere un piano di connessione comune (spesso bene nelle alimentazioni).