# Tips presi dal corso Advanced Hardware Deisgn

## Lezione 1
1. Per iniziare la progetazione è necessario basarsi sull'utilizzo di un reference schematic
che fornisce delle linee guida per la progettazione.
2. Inoltre importare gli schematici forniti dal produttore riduce la possibilità di fare errori perchè molte net sono già presenti
e anche i componenti sono gà creati.
3. Tentare di analizzare uno schematico senza note varie -> È molto complicato, bisogna avere sempre note e descrizioni nello schematico.

Tips DDR:
1. Le memorie DDR sono molto critiche e la loro progettazione va gestita bene perchè lavorano ad alte frequenze.
Se succede alcuni segnali si incrociano non è possibile mantenere le tolleranze necessarie, in questi casi si possono fare degli swapp di byte ovviamente seguendo sempre delle regole fissate:
   1. Ci sono diversi gruppi di segnali in cui i byte possono essere swappati

### Crosstalk
I segnali che si muovo in parallelo possono creare delle interferenze a causa del rumore di crosstalk. Un esempio di questo è se si ha una linea di clk affiancata ad una di interrupt, il clk potrebbe influenzare l'altra linea e facendo ricevere degli interrupt errati.
Cosa influisce sul crasstalk:  
Non dipende dalla frequenza ma da altri fattori come:
1. Rise time
2. source voltage
3. Lenght of parallel route: Il crosstalk si accumula lungo la linea, fino ad arrivare ad un massimo oltre il quale non eccede. La distanza in cui raggiunge il picco dipende dal rise time (più è lento il rise maggiore sarà la lunghezza delle piste che posso avere)
4. Substrate height: Zona di dielettrico sotto le piste che influenza molto la tensione accoppiata (deve essere piccolo).
5. Trace spacing: Distanza tra tracks che alcuni calcolano tra il centro delle piste e altri la distanza tra i bordi interni -> La scelta migliore non la so ancora.
6. Substrate dielectric

## Lezione 2
### Impostazione dello schematico
Gestione dello schematico:
1. Nella prima pagina mettere titolo e descrizione del progetto seguito da uno status per chiarire a che punto è il progetto. Stati di avanzamento:
   1. DRAFT
   2. PRELIMINARY
   3. CHECKED
   4. RELEASED
2. Nella seconda pagina inserie un indice con tutte le pagine dello schematico per semplificare la ricerca e una legenda che chiarisca il significato delle note.
3. Nella terza pagina è bene mettere uno schema a blocchi per capire il funzionamento generale board che si sta progettando.

### PCB design tips
Alcune linee guida generali per un buon PCB:
1. Le piste di alimentazione devono avere sempre il segno '+' e la tensione a cui sono.
   1. Questo da il vantaggio di avere nella sezione "Nets" tutte le power raggruppate all'inizio.
   2. Usanto le white track è poi semplice identificare le varie track nel circuito.
2. Utilizzo delle classi
   1. Utili in generale per imporre le regole durante lo sviluppo del layout.
   2. Molto utili per definire ogni interfacia, questo aiuta molto durante il processo di layout.
3. Utilizzare i testi NF (not fitted) per rendere facile la visualizzazione dei componenti non montanti per le specifiche varianti.
4. Utilizzare il collegamento con il lable delle pagine per rendere semplice la navigazione.
5. Test points: Utili successivamente per fare delle misure comode, in generale utili su linee di potenza e su segnali critici.

### Schematics tips
Alcuni tips per lo schematico:
1. Usare le resistenze opzionali: Mettere sempre lo spazio per le reistenze quando sono opzionali, se in fututo serviranno non dovrà essere fatto un filamento che è poco professionale.
2. Prestare attenzione alle connesioni delle net. La cosa migliore è quella di tenere i componenti su punti diversi di una net comune con giunzioni diverse (molto più leggibile).
3. Mettere tutto nello schematico: posizionare tutte i componenti nello schematico e alla fine decidere come allocare al meglio tutto.
4. Nome delle Net: Nominare le Net più importanti è molto comodo per identificare durante il layout le net a cuibisogna prestare attenzione (quelle di feedback ad esempio che devono essere molto corte).
5. Mechanical: Spesso si aggiungono componenti come dei fori, viducial, ecc. questi non cono dei veri componenti ma vanno nello schematico perchè nella BOM devono esserci tutti quegli elementi necessari a produrre la PCM. Tra le varie cose bisogna aggiungere la PCB che stessa che si sta sviluppando. Se si aggiungono queste cose nello schematico come dei mechanical la BOM sarà pronta senza modifiche manuali.