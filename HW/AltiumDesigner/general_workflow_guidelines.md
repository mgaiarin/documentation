# Progettazione HW
Breve riassunto per iniziare la progettazione di un PCB.

## Setup Altium
1. Per prima cosa istallare Altium Designer
2. Setup iniziale AD. [wiki ref](http://davewiki.lan.dave.eu/wiki/index.php/Sviluppo/CAD_Elettronici/Altium/Altium_Designer)
   1. Accedere su AD con credenziali DAVE per accedere alla licenza (E prender il controllo della licenza).
   2. Aggiungere il workspace DAVE.
   3. Aggiungere le librerie che si trovano in (`R:\devel\_cad\Altium\component_library`).
      1. Selezionare `DAVE_Database_libs.DbLib`
      2. Selezionare `DAVE-UNIV-SYMBOLS_Integrated_Lib`

## Altium Project
1. Creare un nuovo progetto
   1. Selezionare Local Projects
   2. Inserire il nome del progetto
   3. selezionare il path su R: (es. Adapter SDV03 `R:\devel\dave\SDV03\prod\bdc\S-DMTSDV03USB\hw\schematics\`).
   4. Aggiungere alcuni parametri:
      1. release
      2. boardname
      3. (Questa operazione può essere fatta di seguito con tasto destro sul progetto `Project Options -> Parameters`).
2. Aggiungere il progetto a Altium 365 (Selezionare "Make Available  Online).

### Schematico
Per prima cosa iniziare la parte di progettazione HW dallo schematico.

1. Creare e includere nel progetto uno schematico per ogni sezione del progetto (Suddividere bene i fogli in modo che siano organizzati sotto una certa logica comune).
2. Impostare il cartiglio con (`Design -> Sheet Templates -> Server -> Ax template DAVE (v.x)`) e assicurasi di selezionare l'inserimento delle variabili dove c'è il match (quindi release e boardname).
3. Schematico:
   1. Iniziare facendo la "Cover" in cui si inseriscono Titolo del progetto e la PCB.
   2. Nella seconda pagina inserire uno schema a blocchi con qualche informazione sulla comunicazione (usando i drawing tools).
   3. Iniziare la progettazione HW.

### PCB
Appena lo schematico è pronto passare alla progettazione della PCB.
1. Per prima cosa creare il nome della PCB seguendo le info in [wiki](http://davewiki.lan.dave.eu/wiki/index.php/PRODUZIONE_DOC_-_CODICI_PCB).
2. Aggiungere il PCB al progetto prendendo il template in `R:\devel\_cad\Altium\templates\PCB_Templates`. Questo template ha già tutti i Layer configurati per funzionare con gli OutputJobs file. Si noti che ci sono dei Layer che potrebbero servire solo per il quadrotto (es. Dimensions, Cartiglio), se non servono rimuovere le primitive e lasciare i layer vuoti.
3. Preparare lo stack up: Questa fase è critica per molte ragioni:
   1. Se ci sono comunicazioni particolari (es. linee differenziali)
      * Le linee differenziali hanno bisogno di almeno 4 stackup altrimenti lo spessore della board non è sufficiente per avere l'impedenza controllata.
      * Vedere ulteriori casi.
   2. Dipendenza dal produttore che si sceglie:
       1. [JLCPCB](https://jlcpcb.com/): Fornisce Stackup fissi quindi bisogna attenersi a quello che dice
          * Da questo sito andare su "Cababilities" e vedere le informazioni sul prodotto fornite (es. PCB specs, clearence, ecc.).
       2. Altri fornitori: Quando c'è più scelta vedere la pagina [wiki](http://davewiki.lan.dave.eu/wiki/index.php/Tecnica_-_PCB_-_Archivio_stackup_omologati#4_layers) in cui ci sono stackup standard con informazioni varie (es. impedenze controllate).
4. Configurare correttamente tutte le regole in modo da non doversi preoccupare di errori su piste, clearence, vias. ecc.
   1. Questa fase va gestita anche considerando il produttore scelto che in generale fornisce informazioni sulla tecnologia produttiva che ha.

### Panel
Per la produzione dei PCB è necessario crare un pannello in cui vegono piazzati i PCB con l'aggiunta di informazioni aggiuntive.
Anche in questo caso usare il template in `R:\devel\_cad\Altium\templates\PCB_Templates`, il cartiglio è già pronto quindi basterà aggiungere le informazioni sui layer.
### Output files
La generazione della documentazione finale è gestita tramite questa pagina wiki [outputJob](http://davewiki.lan.dave.eu/wiki/index.php/Tecnica/Hardware/Masterizzazione/Altium/Altium_Designer/OutJob). I template sono stati preparati per funzionare con una specifica serie di Layer (Pairs e meccanici) ben precisa, per questo motivo si usa il template 
1. In Altium gli outputJob sono gestiti tramite templates che si possono trovare in `R:\devel\_cad\Altium\templates\OutputJob_Templates`.
2. Copiare i template sulla cartella del progetto.
3. Include i template al progetto. Ogni template definisce il documento da stampare e dove stamparlo.
   1. Schematic_BOM:
      1. BOM:
         1. Salvare i formato `.csv`
         2. Gestire le variazioni direttamente dal template
         3. Salvare in `..\schematic\release\<revision>+'-'+<boardname>\<Output Type>\<VariantName>`
      2. Schematico:
         1. Salvare a colori e Landscape
         2. Salvare in: `..\schematic\release\<revision>+'-'+<boardname>\<VariantName>+'_'<revision>`
      3. Esempio path `..\schematic\release\1.0.0-CS123456\BOM\`
   2. Assembly:
      1. PDF_Assem_view:
         1. In scale di grigio
         2. Pagina TOPAssembly Drawing:
            1. Top Assembly
            2. Keep-Out Layer
            3. Top Designator -> Se non presente andare sulla PCB e selezionare `Tools -> Add Designator for Assembly Drawing`
         3. Pagina BOTTOMAssembly Drawing:
            1. Bottom Assembly
            2. Keep-Out Layer
            3. Bottom Designator -> Se non presente andare sulla PCB e selezionare `Tools -> Add Designator for Assembly Drawing`
      2. File pick and place
         1. Devono esserci le viste top/bottom sia in mm che mils
         2. Ci sono selezionati:
            1. Center-X
            2. Center-Y
            3. Designator
            4. Footprint
            5. Layer
            6. Rotation
   3. Manufacturer_PCB_XL:
      1. File delle varie viste:
         1. CS_all_view
         2. CS_panel_view
         3. CS_component_view
         4. CS_layout_view
      2. Gerber
         1. AST
         2. ASB
         3. ASM
      3. NC Drill Files
      4. ODB++ Files
   4. Mechanical
      1. STEP Files
      2. AutoCad dwg

#### Info template
Informazioni varie sui template (Nota PCB e quadrotto condivino i layer poi la scelta del file da utilizzare va fatta negli outputjob file).  
Layer meccanici:
1. Libreria componenti
   1. Top Assembly: M1
   2. Bottom Assembly: M2
   3. Mechanical 20: Ingombro
   4. Mechanical 21: 3D
   5. La libreria DAVE-MECHANICAL(TP-FID-LABEL) usa i layer meccanici 3-10 per definire alcuni componenti (DA FIXARE!!!)
2. Layers Pairs
   1. Top Assembly: M1
   2. Bottom Assembly: M2
   3. Top Designator: M3
   4. Bottom Designator: M4
3. Mechanical
   1. Board Shape: M5
   2. TitleBlock: M6
   3. Dimension: M7

## Adempiere
Per inserire un nuovo componente (es. la PCB) nel server interno è necessario codificarlo su Adempiere.  
Per adempiere usare la [VM dedicata](http://davewiki.lan.dave.eu/wiki/index.php/Compiere_Manuale_Utente/Utilizzo_con_VM_VirtualBox), vedere anche il [manuale utente](http://davewiki.lan.dave.eu/wiki/index.php/Compiere_Manuale_Utente).  
Step per codicare un nuovo componente:
1. Andare su Codifica Nuovo Prodotto.
2. Inserire i tre ID (es. PCN140R) e segnarsi il codice ottenuto.
3. Completare i campi nella nuova schermata (guardando anche altre pagine simili)
4. Salvare. Alle 4 del mattino lo script aggiorna il file excel.