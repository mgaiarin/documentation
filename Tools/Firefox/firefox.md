# Useful setup firefox
Per poter configurare l'interfaccia Firefox è possibile sfruttare stylesheet.
L'operazione non è banale ma seguendoo le istruzioni in questo [repo](https://github.com/MrOtherGuy/firefox-csshacks) si possono usare file css già definiti che rendono la personalizzazione semplice.  
Una feature in particolare risulta molto comoda, ovvero la possibilità di inserire le tab di firefox su più righe invece di avere l'effetto slide usato di default.

## Setup di base
Setup preferenze per caricare stylesheet:
1. Aprire Firefox e andare su `about:config`.
2. Settare `toolkit.legacyUserProfileCustomizations.stylesheets` su `true`.

Usare gli stylesheet:
1. Cerca la cartella `profile user`
   1. Andare su: `about:support`.
   2. Cercare `Profile Folder`.
   3. Clickare su `Open Folder`.
2. In questa cartella clonare il repo: `git clone https://github.com/MrOtherGuy/firefox-csshacks.git chrome`.
3. Copiare i file:
   1. `userChrome_example.css` -> `userChrome.css`
   2. `userContent_example.css` -> `userContent.css`
   3. Questi file sono di esempio e applicano delle modifiche a firefox dopo il riavvio.
4. Per aggiungere funzionalità:
   1. Editare il file `userChrome.css` importando i `.css` di interesse.
   2. Per attivare la multi row importare: `@import url(chrome/multi-row_tabs.css);`.
5. Maggiori info sul repo.