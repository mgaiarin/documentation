# Tools

Si riportano dei comandi utili da usare con tmux

## Commands

Comandi generici:

* Salvare buffer tmux su file:
  * ref [qui](https://unix.stackexchange.com/questions/688216/how-to-get-the-full-console-log-of-my-tmux-session-on-linux)
  * Salvare tutto il buffer `tmux capture-pane -pS - > ~/tmux-buffer.txt`
  * Catturare il buffer in un tempo specifico:
    * Inizio cattura `Prefix :` + `capture-pane -S -`
    * Fine cattura `save-buffer ~/tmux-buffer.txt`
