# WSL

various info about WSL2.

## Attach Serial

These are the steps required to attach a serial from win to WSL2:

* Install usbipd: `winget install --interactive --exact dorssel.usbipd-wi`
* List all peripherals: `usbipd list`
* Bind serial (change x-y value from what read on previous command): `usbipd bind --busid x-y`
* Check bind operation: `usbip list`
* Attach to WSL: `usbipd attach --wsl --busid x-y`

Official doc [here](https://learn.microsoft.com/en-us/windows/wsl/connect-usb)
