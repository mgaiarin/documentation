# uSD Switch

Riporto dei comandi utili per per usare lo switch uSD

## Setup

### Installazione

```typescript
git clone https://git.tizen.org/cgit/tools/testlab/sd-mux
sudo apt-get update && sudo apt install libftdi1-dev libpopt-dev cmake
cd sd-mux
mkdir build && cd build
cmake ..
make
sudo make install
```

## Conf

```typescript
sudo sd-mux-ctrl
sudo sd-mux-ctrl --device-serial=sd-wire_01-100 --vendor=0x04e8 --product=0x6001 --device-type=sd-wire --set-serial=sd-wire_01-100
sd-mux-ctrl-list
```

## Comandi

```typescript
# Switch to DUT
sudo sd-mux-ctrl -v 0 -d
# Switch to VM
sudo sd-mux-ctrl -v 0 -s
# Comando utile per velocizzare operazioni
sudo sd-mux-ctrl -v 0 -s && read && ls /dev/sda1 && cp pippo /media/dvdk/boot && umount /dev/sda1 && sudo sd-mux-ctrl -v 0 -d
sudo sd-mux-ctrl -v 0 -s && read && ls /dev/sdaa && dd if=flash.bin of=/dev/sda bs=1k seek=33 conv=fsync
```
