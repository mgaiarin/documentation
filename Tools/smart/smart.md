# Smart Package Manager SMART

Gestore di pacchetti utile per eseguire installare appllicativi di test su boards. Il manuale si puó trovare a questa pagina [qui](https://linux.die.net/man/8/smart).  
Informazioni aggiuntive si possono trovare a in [questo link](https://labix.org/smart/howto#head-0de3ec3dcf5b48b5384719a7221633d01f1693ab).

## Utilizzo

Per funzionare é necessario aggiungere per prima cosa il repository da cui scericare i pkg. Per DAVE si trova [qui](http://yocto.dave.eu/).

* Aggiungere il canale: `smart add channel <label> type=<type> name="Repo name" baseurl=<url/to/the/channel>`
  * Il tipo di repository utilizzati in azienda sono: `rpm-md`, `rpm-sys`, `apt-deb` e `apt-rpm`
* Update della cache: smart update
* Installazione pacchetto: smart install <nome package>

Esempio di utlizzo: `smart channel --add xelk-3.0.1 type=rpm-sys name="Xelk-DAVE" baseurl=http://yocto.dave.eu/xelk-3.0.1/cortexa9hf_neon/`

## Troubleshoot

Problemi comuni:

* Dipendenze mancanti: verifcare se il pacchetto ha delle dipendenze in un canale diverso. In questo caso aggiungere il canale e fare un update.
* Verificare la connesione internet.
