# Altium

* È teoricamente possibile usare un tool che elimina automaticamente il tutti i silkscreen che sono fuori regola
* Quando si imposta l'impedenza controllata e meglio avere un valore maggiore del target rispetto ad uno minore ma più vicino
  * Questo perché i segnali lavorano sui fronti e se l'impedenza è maggiore al massimo hai un elongazione maggiore
  * Invece se hai un impedenza minore rischi di ritrovarti un gradino che è diffcile da valutare
  * e.g target a 50ohm
    * Imposto 55: male che vada ho 60ohm -> fronti lunghi ma il sistema funziona
    * Imposto 49.9: male che vada ho 45ohm -> e questo potrebbe essere un grosso problema
* Ad oggi le resistenze di terminazione si usano poco
  * Questo perché una volta si usavano stackup a 2 alyer e non era possibile avere piste adattate
  * Con la tecnologia odierna puoi avere delle geometrie adattate e poi fare a meno di resistenze di terminazione
  * 

## Tool esterni utili

* Ci sono tool (a pagamento) che dai gerber sono in grado di identificare problemi di disattamento di impedenza.
