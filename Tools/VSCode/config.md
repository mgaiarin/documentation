# VSCode config

Si riportano utili configurazioni VSCode per il lavoro

## Estensioni

Utili estensioni per il lavoro quotidiano:

* C/C++ Themes
* Bookmarks
* Dev Containers
* DeviceTree
* Draw.io integration
* LinkerScript
* PRACTICE
  * Utile tool per la syntax di Lauterbach
* ProjectManager
* Remote SSH
* Remote SSH: Editing Configuration FIles
* Remote Explorer
* TODO Highlight
* vscode-icons
* WSL
* bitbake
* C/C++
* C/C++ Extension Pack
* CMake
* CMake Language Support
* CMake tool
* Code Runner
* Docker
* GitGraph
* GitHistory
* Git Project Managaer
* GitLab Workflow
* GitLens
* HexInspector
* Indent-rainbow
* Markdown All in One
* Path Intellisense
* Start git-bash

## Config

* Evitare l'accesso dell'explorer in cartelle che contengono molti file
  * Preferences: Open workspace/user settings
  * Features -> Explorer
  * File Exclude -> Inserire il path alle cartelle da escludere