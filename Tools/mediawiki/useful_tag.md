# Mediawiki tag

Raccolta di codice HTML utile per scrivere documentazione in mediwiki.

* Codeblock evidenziato ed espandibile:

```html
<div class="toccolours mw-collapsible mw-collapsed">
Expand code:
<div class="mw-collapsible-content">
<syntaxhighlight lang="bash" line">

code

</syntaxhighlight>
</div>
</div>
```

* Per inserirlo in una lista e avere l'indentazione corretta va tutto inline:
  * Il messaggio custom non è ben visualizzato

```html
<br><div class="toccolours mw-collapsible mw-collapsed"><div class="mw-collapsible-content"><syntaxhighlight lang="bash" line">

code

</syntaxhighlight>
</div>
</div>
```

* Galleria con più immagini inserite inline:

```html
<gallery widths=300px heights=300px mode="nolines">
File:Example.jpg|Lorem ipsum
File:Example.jpg|Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
</gallery>
```

* Scritte in grigetto

```html
<div><span style="color:#72777d">

</span></div>
```
