# SSH setup

Riporto utili comandi per ssh.

## Creazione chiavi RSA

Torna spesso utile creare le chiavi con formato RSA. Di seguito il comando per la generazione.

```bash
ssh-keygen -t rsa -C "myname@mycompany.com" -f myname@mycompany.com
```

### Permessi

Si noti che bisogna fornire i giusti permessi per i file ottenuti:

* Cartella `.ssh`: `chmod 700 ~/.ssh/`
* Public key: `chmod 644 ~/.ssh/id_rsa.pub`
* Public key: `chmod 600 ~/.ssh/id_rsa`

## File di configurazione

Molto utile avere in `.ssh` il file di configurazione per ogni server/VM/macchina in cui collegarsi, per usare la connessione tramite chiavi.  
Ogni entri ha questo tipo di struttura:

```text
Host git.dave.eu
    User git
    Hostname git.dave.eu
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/myname@mycompany.com
```

Tramite quest file poi ci si potrà collegare semplicemente usando il nome definito dopo `Host` e la confiurazione verrà presa dal file, es. `ssh git.dave.eu`.

## Copiare chiave pubblica

Molto utile sulle VM/server è la possibilità di aggiungere la chiave pubblica per potersi autenticare con quella invece che con la password.  
Questo si può fare molto semplicemente con il seguente comando:

```bash
ssh-copy-id -i ~/.ssh/id_rsa.pub YOUR_USER_NAME@IP_ADDRESS_OF_THE_SERVER
```
pr
