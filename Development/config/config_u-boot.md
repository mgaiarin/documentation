# U-boot configuration

Guida per il setup ddi u-boot con informazioni utili per capire le variabili utilizzate. Si noti che le informazioni sono in fase di sviluppo e da aggiornare. Inoltre molte variabili inserite fanno riferimento al DESK-MX-L, in casi diversi alcuni parametri possono differire.
Prendere di riferimento anche le guida uffciali:
1. [u-boot](https://u-boot.readthedocs.io/en/stable/index.html).
2. [u-boot reference manual](https://hub.digi.com/dp/path=/support/asset/u-boot-reference-manual/).
3. Utile guida anche se [unofficial](https://www.campana.vi.it/blog/spiegazione-parametri-uboot/).

## Variabili principali

Tra le variabili principali di u-boot si trova:

1. Config generica di u-boot
   1. `bootcmd`: Comando che esegue all'avvio per fare il boot di Linux.
   2. `baudrate=115200`
   3. `board_name=EVK`
   4. `board_rev=iMX8MP`
   5. `bootdelay=[-1/0/1/..]`: Delay prima di fare il boot linux.
   6. `bootretry= [-1/0/1/..]`: Delay prima di provare a fare un reboot
   7. `ethaddr=00:50:C2:B9:CF:99`: MAC address assegnato alla scheda.
   8. `ip_dyn= [no/yes]`: Definisce se si usa un ip dinamico -> utile per capire se usare DHCP o no.
   9. `soc_type=imx8mp`
2. Generico per eseguire il boot
   1. `bootfile= uImage`: Image da usare per il boot linux.
   2. `boot_fit= [no/yes]`: Specifica se l'immagine utilizzata è di tipo compresso (kernel + dt)
   3. `bootargs=console=ttymxc1,115200 root=/dev/mmcblk1p2 rootwait rw`: Argomenti da usare per il boot.
   4. `fdt_file=<filename.dtb>`: Nome del file del dt da utilizzare.
   5. `fdt_addr=0x43000000`: Indica il l'indirizzo di memoria in cui si trova il dt nella flash.
   6. `loadaddr=0x40480000`: Fa il boot nel binario a quell'indirizzo di memoria (dipende da come è stato compilato il kernel).
   7. `console=ttymxc1,115200`: Setup della seriale di debug.
   8. `get_cmd=tftp`: Comando per usare tftp.
   9. `booti`: Comando per eseguire il boot dell'immagine flat o compressa "Image".
   10. `bootm`: Comando per Caricare l'immagine di tipo FIT al cui interno è gia presente il dt e quindi non bisogna caricarlo a parte.
   11. `fatload mmc <dev>[:partition] <loadAddress> <bootfilename>`: Legge un'immagina da mmc e la carica.
3. Boot da mmc/ram
   1. `mmcdev=1`: Specifica il device da usare
   2. `mmcpart=1`: Specifica la partizione da usare
   3. `mmcroot=/dev/mmcblk1p2 rootwait rw`: Indica il root della mmc da cui prendere kernel e dt.
   4. `mmcargs=setenv bootargs ${jh_clk} console=${console} root=${mmcroot}`: Setup della variabile `bootarg` per il boot da mmc.
   5. `mmcboot`: Comando per avviare il boot da mmc.
   ```
   mmcboot=echo Booting from mmc ...; run mmcargs; if test ${boot_fit} = yes || test ${boot_fit} = try; then bootm ${loadaddr}; else if run loadfdt; then booti ${loadaddr} - ${fdt_addr}; else echo WARN: Cannot load the DT; fi; fi;
    ```
4. Boot da rete
   1. `nfsroot=`: Root del server da utilizzare (vedi istruzioni dopo).
     * `rootpath=/pat/to/the/fs/`: Path in cui e presente il fs (Forse da usare su memoria?).
   2. `serverip`: Indica l'IP del server da cui prendere kernel e fs.
   3. `gatewayip`:
   4. `netmask`:
   5. `hostname`:
   6. `netdev=ethX`: Indica l'ethernet da usare.
   7. `netargs`: Setta la variabile `bootargs`  per fare il boot da rete.
   ```
   netargs=setenv bootargs ${jh_clk} console=${console} root=/dev/nfs ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}:${netdev}:off panic=1 nfsroot=${serverip}:${nfsroot},v3,tcp debug loglevel=8
   ```
   8. `netboot`: Definisce i comandi per eseguire l'avvio da nfs.
   ```
   netboot=echo Booting from net ...; run netargs;  if test ${ip_dyn} = yes; then setenv get_cmd dhcp; else setenv get_cmd tftp; fi; ${get_cmd} ${loadaddr} ${bootfile}; if test ${boot_fit} = yes || test ${boot_fit} = try; then bootm ${loadaddr}; else if ${get_cmd} ${fdt_addr} ${fdt_file}; then booti ${loadaddr} - ${fdt_addr}; else echo WARN: Cannot load the DT; fi; fi;
   ```
5. Boot microprocessore (caso M7)
   1. `m7_bootaddr=0x40000000`
   2. `m7_file=dmx.bin`
   3. `m7_loadaddr=0x48000000`
   4. Boot da ftp
      * `m7_boot=tftp 0x80000000 ${m7_file}; dcache flush; bootaux 0x80000000`

## Boot conf utili

Si riportano una serie di utili configurazioni da usare in fase di debug.

* Boot da rete
  * `run net_nfs`

   ```bash
   setenv net_nfs "run loadk loadfdt nfsargs addip addcons addmisc; if run configid_fixupfdt; then bootm ${loadaddr} - ${fdtaddr}; fi"
   setenv loadk "tftpboot ${loadaddr} ${serverip}:${bootfile}"
   setenv loadfdt "tftpboot ${fdtaddr} ${serverip}:${fdtfile}"
   setenv nfsargs "setenv bootargs root=/dev/nfs rw nfsroot=${serverip}:${rootpath},v3,tcp"
   setenv addip "setenv bootargs ${bootargs} ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}:${netdev}:off panic=1"
   setenv addcons "setenv bootargs ${bootargs} console=${console},${baudrate}"
   setenv addmisc "setenv bootargs ${bootargs} vmalloc=400M ${mtdparts}\;${mtdparts_spi}"
   setenv bootfile uImage
   ```

* Boot da mmc
  * `run mmc_andr`

  ```bash
   setenv nfsargs "setenv bootargs root=/dev/nfs rw nfsroot=${serverip2}:${rootpath2},v3,tcp"

   configid_fixupfdt
  ```

* Parametri di caricamento
  * Load addr uImage: `setenv loadaddr 0x12000000`
  * Load addr fdt: `setenv fdtaddr 18000000`
  * rootpath: `setenv rootpath /opt/nfsroot/<nfs_necessario>`
  * fdtfile: `setenv fdtfile tftpboot/<fdt_necessario>`
  * bootfile `setenv fdtfile tftpboot/<bootfile_necessario>`

## Path fs

Tutti i file sistem si trovano in `R:\opt\nfsroot\<project_name\<fs_version>`.  
Di seguito una lista dei path per i progetti specifici:
1. SALCR: `R:\opt\nfsroot\salcr\rfs-BDC-1.0.1`

## Useful commands

Comandi utili per fare diagnosi con u-boot:

* `fatls mmc 0:1`: legge da uSD partizione 1
* `configid`: Serve a configurare il configid
  * `configid som_configid 00000001`: Scrive nell'OTP usato per il som_configid
  * Se non è settato bisogna usare uno u-boot di devel e impostarlo, altrimenti u-boot non funzionerà
* `mmc`: [ref qui](https://u-boot.readthedocs.io/en/latest/usage/cmd/mmc.html)
  * `info`
  * `mmc read addr blk# cnt`
  * `mmc write addr blk# cnt`
  * `mmc erase blk# cnt`
  * `mmc rescan [mode]`
  * `mmc part`
  * `mmc dev [dev] [part] [mode]`
  * `mmc list`
  * `mmc wp`
  * `mmc bootbus <dev> <boot_bus_width> <reset_boot_bus_width> <boot_mode>`
  * `mmc bootpart-resize <dev> <dev part size MB> <RPMB part size MB>`
  * `mmc partconf <dev> [[varname] | [<boot_ack> <boot_partition> <partition_access>]]`
  * `mmc rst-function <dev> <value>`
* Aggiornamento u-boot su uSD/eMMC:
  * Se è presente `u-boot.img` + `SPL`:
    * Generic

    ```bash
      setenv spl axel/u-boot/xelk-4.0.0_mx6qdlaxel_devel_SPL
      setenv ubootimg  axel/u-boot/xelk-4.0.0_mx6qdlaxel_devel_u-boot.img
    ```

    * XELK:

    ```bash
      setenv load_spl "tftp ${loadaddr} ${spl}"
      setenv load_uboot "tftp ${loaddr} ${ubootimg}"
      setenv spi_update_spl "sf probe; sf erase 0 10000;sf write ${loadaddr} 400 ${filesize}"
      setenv spi_update_uboot "sf probe; sf erase 10000 f0000;sf write ${loadaddr} 10000 ${filesize}"
    ```

    * DESK-MX-L (to check):

    ```bash
      setenv load_spl "tftp ${loadaddr} ${spl}"
      setenv load_uboot "tftp ${loaddr} ${ubootimg}"
      setenv mmc_update_spl "mmc dev; setexpr blocks ${filesize} / 0x200; setexpr blocks ${blocks} + 1; mmc write ${loadaddr} 2 ${blocks}"
      setenv mmc_update_uboot "mmc dev; setexpr blocks ${filesize} / 0x200; setexpr blocks ${blocks} + 1; mmc write ${loadaddr} 0x8a  ${blocks}"
    ```

  * Se è presente `u-boot.imx`:
    * `setenv load "tftp ${loadaddr} ${uboot}"`
    * `setenv mmc_update "mmc dev; setexpr blocks ${filesize} / 0x200; setexpr blocks ${blocks} + 1; mmc write ${loadaddr} 2 ${blocks}"`
  * Installare u-boot su nand:
    * XELK [TBD]

## RFS utili

Utili configurazioni di default

* SDV03

```bash
setenv fdtfile sdv03/linux/sdvx-1.0.5_lite_imx6dl-sdv03-cb002a.dtb
setenv bootfile sdv03/linux/sdvx-1.0.5_lite_uImage
setenv rootpath /opt/nfsroot/sdvx/sdv03-20181025
setenv serverip 192.168.0.13
setenv ipaddr 192.168.0.163
run net_nfs
```

* SZNPC

```bash
setenv fdtfile sznpc/linux/sznpc-1.3.1_imx6dl-sznpc-cb0054.dtb
setenv bootfile sznpc/linux/sznpc-1.3.1_uImage
setenv rootpath /opt/nfsroot/sznpc/sznpc-1.1.0_3/sznpc-image-qt4-sznpc
setenv serverip 192.168.0.13
setenv ipaddr 192.168.0.163
run net_nfs
```