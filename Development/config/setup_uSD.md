# Creazione di una uSD

Quando non è disponibile un build Yocto che genera un file flashabile direttamente su uSD o se si vogliono provare sviluppi senza fare la build è necessario farsi la uSD manualmente.  
Per avere una uSD in grado di far partire una scheda sono necessari:

1. ATF (ARM Trusted Firmware)
   * Codice iniziale che serve ad avviare le prime istruzioni della board
2. U-boot
3. Immagine Linux
4. RFS

Da primi due punti è possibile ottenere un'immagine in grado di eseguire il boot della scheda. Gli altri due servono per avviare Linux in maniera corretta.
Verranno mostrate di seguito le linee guida per buildare e preparare correttamente una uSD. Si prende come caso di esempio `DESK-MX-L` ma le stesse operazioni possono essere seguite anche per altri sistemi.

## Creazione di una uSD con mkimage

Guida al setup di una uSD per far partire una board e utili configurazioni per u-boot.

### Clonare i sorgenti necessari

Per prima cosa bisogna

Put-it-all-together tool to generate bootable DESK-MX8M-L images

Clone this repository with submodule:

```
git clone --recursive git@..../desk-mx8-mkimage.git
```

or if this doesn't work

```
git clone git@..../desk-mx8-mkimage.git
git submodule update
git submodule upgrade
```

### Build U-Boot

As simple as:

```
cd u-boot-imx
yabt build
```

Please note that this builds the default U-Boot target/configuration. You may need to specify a different target.

### Build ATF

As simple as:

```
cd imx-atf
yabt build
```

Please note that this is usually a generic command that generates all required binaries (kernel + device trees) for the different platform.

In other words: it is not usually required to specify a different target depending on specific board configuration.

### Get iMX firmware

Just run

```
./fetch_firmware.sh
```

This will download the binary archive, **accept EULA automatically** (you must agree with that) and extract the archive itself

### Generare boot image

After building the single pieces of software, run:


```
./${BOARD}_generate_flash.bin
```

Where `BOARD` is:
* `imx8mm-mito8mmini`
* `imx8mp-mito8mplus`

### Flash uSD

In the end is necessary to flash the uSD with the binary created. This bynary must be flash as row, so no partition must be present. run:

```
sudo dd if=flash.bin of=/dev/sdx bs=1k seek=32; sync
```

Note that the flag seek depends on the system is used, so check it before flash.

### Linux partition

TBD

## Creazione di una uSD con script mksd.sh

Sistemi più vecchi da cui la build fornisce solo u-boot, kernel e dtb richiedono una procedura diversa per ottenere una uSD bootabile.  
Di seguito una guida con gli script utili per la generazione, fare anche riferimento alle pagine:

* [DESK-MX6UL-L/Development/How to create a bootable microSD card](https://wiki.dave.eu/index.php/DESK-MX6UL-L/Development/How_to_create_a_bootable_microSD_card).
* [How to create a bootable microSD card (XUELK)](https://wiki.dave.eu/index.php/How_to_create_a_bootable_microSD_card_%28XUELK%29).
* [How to create a bootable microSD card (XELK)](https://wiki.dave.eu/index.php/How_to_create_a_bootable_microSD_card_(XELK))

Step per la generazione della uSD:

* Per prima cosa scrivere lo script per generare la uSD:
  * Si noti che gli script per ottenere la corretta configurazione dipendono anche dal tipo di u-boot di cui si dispone:
    * `u-boot.img`: Richiede che ci sia anche l'SPL
    * `u-boot.imx`: Possiede un header e quindi non è richiesto l'SPL
  * Di seguito gli script da usare nei due casi
    * Usare il seguente script nel caso si abbia `u-boot.img`:
      * [mksd_img.sh](../files/mksd_img.sh)
    * Usare il seguente script nel caso si abbia `u-boot.imx`:
      * [mksd_imx.sh](../files/mksd_imx.sh)
* Successivamente generare il `bootscript.txt` (modificarlo in base alle esigenze):
  * [bootscript.txt](../files/bootscript.txt)
* Creare una cartella in cui saranno presenti:
  * Lo script `mksd.sh`
  * u-boot
  * SPL
  * la cartella `bin/` in cui mettere:
    * Kernel
    * DTB
    * bootscript: `boot.scr`
  * RFS
* Infine eseguire lo script `mksd.sh` e passare come parametri:
  * Il device dove installare il sistema (es. /dev/sdb).
  * L'immagine u-boot
  * l'SPL (se necessario)
  * Il path alla cartella con i binari: `bin/`
  * Il RFS
* A questo punto verrà preparata una uSD bootabile.