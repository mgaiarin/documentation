# Configurazione interfaccia di rete

Mini guida per un setup veloce delle connessioni ethernet (Sia per VM che board).

## Configurazione tramite NetworkInterfaces

Questo tipo di configurazione si basa sull'editig di file di configurazione Linux.  
Se si usa una VM per prima cosa bisogna creare la connessione internet `Bridge Adapter` che poi verrà riconsciusta di linux come un interfaccia ethernet. Da questo momento le modifiche da fare sono comuni per VM e board generiche.  
Step da seguire:

1. Editare il file delle interfaccie /etc/network/interfaces aggiungendo le seguenti configurazioni (modificare le variabili `X` e `abc`):
```vim
auto ethX
iface ethX inet static
address 192.168.0.abc
netmask 255.255.255.0
broadcast 192.168.0.0
gateway 192.168.0.254
```

   * Nel caso in cui si stia usando `nfs` il gateway non viene inizializzato perchè l'interfaccia ethX dovrebbe essere spenta e poi riaccesa, non si può fare. In questi casi usare il comando `route add default gw 192.168.0.254 dev ethX` per aggiungere il default gw (va fatto ad ogni riavvio).
  
2. Eseguire il comando `echo "nameserver 192.168.0.1" | resolvconf -a eth0` per la risoluzione del dominio interno.
   * Per eseguire manualmente editare `/etc/resolvconf/resolv.conf.d/base` inserendo la stringa precedente e riavviare il servizio con `sudo service resolvconf restart`
   * Se resolveconf non é disponibile editare `/etc/resolv.conf` inserendo la stringa precedente.

## Configurazione tramite systemd

Se per configurare la rete si usa systemd evitare di configurare resolvconf e interfaces.  
Il vantaggio portato da questo tipo di configurazione è che si basa sull'utilizzo dei servizi, questo permette quindi di avere dei feedback su quello che succede tramite il comando `systemctl status <servicename>.  

Di seguito verranno mostrati i passi seguiti per configurare la scheda CloudIO. Le configuazioni varia in base alla rete utilizzata, maggiori informazioni di systemd-networkd nella pagina wiki dedicata a [questo link](https://wiki.archlinux.org/title/systemd-networkd).  
Configurazione:

1. Creare il file `/etc/systemd/network/20-eth0.network`
```vim
[Match]
Name=ethX

# Prevent the interface loading if the kernel boots from nfs
KernelCommandLine=!nfsroot

[Network]
Address=192.168.0.abc/24
Gateway=192.168.0.254
DNS=192.168.0.1
```

2. Se non è presente creare il link: `ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf`.
    - In realtà questa operazione non sembra necessaria.
3. Riavviare il servizio `systemd-resolved`: `systemctl restart systemd-resolved`.
4. Riavviare il servizio `systemd-networkd`: `systemctl restart systemd-networkd`.
5. Riavviare la macchina.

## Configurazione tramite nmcli

Se è necessario configurare tramite `nmcli` seguire i seguenti passi:

* Verificare le connessioni disponibili: `nmcli con show`
  * Verificare i nomi delle connessioni
  ```bash
  root@cloudio-cm7dbg:~/test_target# nmcli con show
   NAME                UUID                                  TYPE      DEVICE
   Wired connection 2  2a016a43-2432-3d57-96a7-61df45425c7b  ethernet  eth1
   Wired connection 1  780ca5bd-3192-31ee-9a9a-1fea9e118078  ethernet  eth0
  ```
* Eseguire il setup, esempio:
```bash
nmcli con mod "Wired connection 2" ipv4.address 192.168.0.xyz/24
nmcli con mod "Wired connection 2" ipv4.gateway 192.168.0.254
nmcli con mod "Wired connection 2" ipv4.dns "8.8.8.8"
nmcli con mod "Wired connection 2" ipv4.method manual
nmcli con up "Wired connection 2"
```

