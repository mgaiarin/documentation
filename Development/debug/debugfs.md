# DEBUGFS

Un utile strumento di debug del kernel Linux è `debugfs`, con la quale si possono tracciare le chiamate alle funzioni dei driver e fare test su specifiche periferiche.  
Si riportano quindi varie configurazioni provate per il debug e analisi del kernel.

## usbmon

Per testare il funzionamento delle USB e verificare la presenza di errori durante il trasferimento si può usare ilmodulo `usbmon`, il quale si basa su `debugfs`.  

* Fare riferimento alla documentazione [usbmon doc](https://docs.kernel.org/usb/usbmon.html)
* È necessario buildare il kernel con i moduli
  * `CONFIG_USB_MON`
  * `CONFIG_DEBUG_FS`
* Eseguire il comando per sniffare i pacchetti USB e copiare su file: `sudo cat /sys/kernel/debug/usb/usbmon/<xy> > bus2data.txt`
  * `<xy>` indica il bus da sniffare, verificare sulla guida per capire quello corretto.
* Verificare i risultati facendo riferimento sempre alla guida ufficiale