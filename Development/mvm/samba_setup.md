# Setup cartella condivisa tra VM e WIN
Guida per creare una cartella condivisa tra la VM linux e l'host Windows.
Questo tipo di condivisione risulta particolarmente utile per poter utilizzare TRACE32 avendo acceso ai sorgenti presenti nella VM.

## Setup Linux
Dalla virtual machine è necessario verificare che samba sia correttamente installato.
Se non è installato  eseguire : `sudo apt install samba`

Verifica servizi:
* `sudo systemctl status samba`
* `sudo systemctl status nmbd`
* `sudo systemctl status smbd`

Modificare i file di configurazione:
```
sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.bak
sudo vi /etc/samba/smb.conf
```

Inserire in smb.conf la seguente configurazione:
```
[global]
workgroup = WORKGROUP
wins support = yes
dns proxy = no
interfaces = ethx 127.0.0.0/8 192.168.100.2/24
bind interfaces only = yes
security = user
map to guest = Bad User
# DVDK's share
[public]
path = /home/dvdk/mito
browseable = yes
guest ok = yes
writable = yes
```

Per mappare tutto il fs:
```
[rootdir]
path = /
browseable = yes
guest ok = yes
writable = yes
```

## Setup Windows

* Abilitare una nuova interfaccia di rete di tipo `Scheda solo host` andando su `VirtualBox -> impostazioni -> Rete`
* Aggiungere la subnet corrispondente andando su `Pannello di controllo -> Network and internet -> Network and sharing center -> change adapter settings `
** Da qui selezionare l'adapter `VirtualBox Host-Only Network -> Protocollo Internet versione 4`
** Aggiungere l'IP 192.168.100.1
** Aggiungere la certella condivisa. Path da utilizzare `\\192.168.100.2\public`
## troubleshoot
Ci possono essere dei problemi con i servizi samba:
* Il servizio `sudo systemctl status smbd` da come output:
```
❯ sudo systemctl status smbd                                                                                            ─╯
● smbd.service - LSB: start Samba SMB/CIFS daemon (smbd)
   Loaded: loaded (/etc/init.d/smbd; bad; vendor preset: enabled)
   Active: active (running) since Wed 2021-11-24 12:40:45 CET; 15min ago
     Docs: man:systemd-sysv-generator(8)
    Tasks: 3
   Memory: 3.1M
      CPU: 123ms
   CGroup: /system.slice/smbd.service
           ├─2735 /usr/sbin/smbd -D
           ├─2737 /usr/sbin/smbd -D
           └─2748 /usr/sbin/smbd -D

Nov 24 12:40:45 vagrant smbd[2735]:   bind failed on port 445 socket_addr = 192.168.100.2.
Nov 24 12:40:45 vagrant smbd[2735]:   Error = Cannot assign requested address
Nov 24 12:40:45 vagrant smbd[2735]: [2021/11/24 12:40:45.184071,  0] ../source3/smbd/server.c:709(smbd_open_one_socket)
Nov 24 12:40:45 vagrant smbd[2735]:   smbd_open_once_socket: open_socket_in: Cannot assign requested address
Nov 24 12:40:45 vagrant smbd[2735]: [2021/11/24 12:40:45.184236,  0] ../source3/lib/util_sock.c:396(open_socket_in)
Nov 24 12:40:45 vagrant smbd[2735]:   bind failed on port 139 socket_addr = 192.168.100.2.
Nov 24 12:40:45 vagrant smbd[2735]:   Error = Cannot assign requested address
Nov 24 12:40:45 vagrant smbd[2735]: [2021/11/24 12:40:45.184470,  0] ../source3/smbd/server.c:709(smbd_open_one_socket)
Nov 24 12:40:45 vagrant smbd[2735]:   smbd_open_once_socket: open_socket_in: Cannot assign requested address
Nov 24 12:47:21 vagrant systemd[1]: Started LSB: start Samba SMB/CIFS daemon (smbd).
```
In questo caso verificare di aver assegnato all'ethx l'IP fisso corretto.
* `sudo systemctl status samba` da come output:
```
Failed to start samba.service: Unit samba.service is masked.
```
In questo caso eseguire i comandi:
```
sudo rm /lib/systemd/system/samba.service 
sudo systemctl enable samba.service nmbd.service
sudo systemctl start samba
```
