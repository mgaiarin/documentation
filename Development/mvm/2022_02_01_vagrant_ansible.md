# Corso Vagrant e Ansible
Ambiente di sviluppo:
1. Una volta si preparava una macchina per lo sviluppo (~2/3 giorni) e se si modificava qualcosa si rischiava di far saltare tutto l'environment.
2. Per passare il lavoro al cliente inizialmente si preparava tutto il PC.

Nuovi flussi di lavoro:
1. Usare server per la build.
2. Preparare una VM preconfigurata per la build.
3. Passare VM già pronta al cliente.

Preparazione delle VM:
1. Vagrant: Sistema per la creazione di VM in maniera automatizzata.
2. Ansible: Sistema di configurazione automatizzato che installa tutti i tool automaticamente.

## Pre-requisiti
1. VirtualBox: Sempre meglio tenere traccia delle versioni su cui una specifica VM funziona correttamente )(da fare su )
2. Vagrant: Installa diversi pacchetti "guest" nella VM che possono essere tracciati dal suo sistema di build. Esempi:
   1. `config.vm.provision :shell, :inline => "pip install \"ansible < 4.0\""`
   2. `config.vm.provision :shell, :inline => "apt-get install -y python python-pip"`

Vagrant e Ansible vengono gestiti come un software normale e quindi tracciati su GitLab.
Non ci sono sistemi di build automatica per ora

## Vagrant
Si configura con file proprietario gestito con ruby.  
Alcuni comandi base per la generazione della macchina che si trovano nello script di configurazione:
1. **config.vm.box: "[Linux version]"**:Nella versione base lui parte da un box (preconfezionata) che fa partire una specifica versione di Linux (es. "bento/ubuntu-20.04")
2. **vb.name = "CloudIO-MVM"** : Configura il nome della macchina
3. **config.vm.network "public_network", ip: settings['ip_address']**: Configura automaticamente la rete
4. **config.vbguest.auto_update = true**: Le guest addition sono quei tool che permettono la coesione tra host, VirtualBox e il guest. Per ridurre la possibilità di errori bisogna installare le GA manualmente con `vagrant plugin install vagrant-vbguest`
5. **ansible.playbook = "ansible/playbook.yml"**: Esegue ansible per andare a configurare tutta la parte guest dell VM

Usando un file `.ylm` che fornisce alcune variabili è possibile ottimizzare alcuni parametri:
1. `ip_address`: Definisce l'ip fisso da settare.
2. `memory`: Definisce la RAM da fornire
3. `cpu`: Definisce il numero di CPU da fornire

Comandi per l'esecuzione:
1. `vagrant up`: Se non esiste la crea, altrimenti la avvia e ne fa il provisioning.
2. `vagrant provision`: Esegue il provision su VM già attiva.
3. `vagrant halt`: Spegne una VM.
4. `vagrant reload`: riavvia una VM.

## Ansible
Sistema di automatizzazione per l'installazione di pacchetti ed esecuzioni di comandi shell su Linux.
C'è una struttura dati che definisce cosa si vuole fare da shell, ma senza specificare il comando direttamente.  
Esempi: 

```[YAML]
# Nome che si da al comando
- name: install my custom packages
  # Indica la primitiva che in funzione della versione linux fa quello che serve
  # Il vantaggio e che non devo mettere il comando specifico che magari su alcune VM non va
  # Inoltre le primitive hanno altre funzioni (es. update, autoclean, ecc)
  apt:
    pkg:
      - zsh
    state: latest
```

Anche in questo caso è possibile definire un file di variabili per settare rapidamente varie informazioni.
Struttura di un role:
1. defaults: Non si sa bene
2. files: Contiene file da utilizzare (esempio da copiare da qualche parte)
3. handler: Contiene gli handler che se si modifica qualcosa in una configurazione lui triggera altre funzioni
4. meta: Mai usato
5. task: Contiene i task principali per l'esecuzione di comandi
6. templates: Simile a "files" ma qui prende un template a cui sostituisce alcune variabili prima di usare il file
7. vars: Contiene file di configurazione con variabili varie.