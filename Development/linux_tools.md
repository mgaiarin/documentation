# Guida sui tools Linux per sviluppo

Questa guida ha lo scopo di tracciare diversi tools Linux per avere informazioni su periferiche/hw generale e poter analizzare lo stato del sistema.
Spesso questi tool si usano quando si fanno analisi sui touchscreen (o sistemi di input vari). La guida verrà aggiornata con l'avanzare del tempo per tenere traccia di tutti i tool utilizzati durante le qualificazioni e sviluppi vari.

## Struttura del fs

Prima di analizzare i tool usati in Linux è meglio avere un'idea migliore di come è strutturato il filesystem. Verranno spiegate alcune delle cartelle più importanti:

1. `/dev/`: Quasi tutti i file presenti in questa cartella sono `device file`. Mentre la lettura e la scrittura di un file normale memorizza i dati su un disco o un altro filesystem, l'accesso a un file di dispositivo comunica con un driver nel kernel, che generalmente a sua volta comunica con un pezzo di hardware (un dispositivo hardware, da cui il nome). Ci sono due tipi di `device file`:
      1. `block devices`: Come le memorie, si comportano come grossi file a dimensione fissa: se scrivi un blocco di memoria in un certo offset e poi vai a rileggere in quel punto troverai i dati che hai scritto.
      2. `character devices`: Scrivere/leggere dei byte in questi file invece ha un effetto immediato, si usano ad esempio per le periferiche.
      3. I device sono definiti da due numeri:
         1. `major`: che indica quale driver è responsabile per quel device.
         2. `minor`: che permette ad un unico driver di pilotare più dispositivi.
      4. Altri device sono fittizi e forniscono solo alcune funzioni utili come:
         1. `null`: Scrivendo o leggendo da qui non succede nulla
         2. `zero`: Fornisce bytes di `null` all'infinito
         3. `urandom`: Fornisce byte random all'infinito
      5. Il significato di alcuni device invece dipende dal processo che gli usa, come:
         1. `stdin`: Definisce lo standard input del processo corrente; aprirlo ha all'incirca lo stesso effetto di aprire il file originale che è stato aperto come standard input del processo.
         2. `tty`:  designa il terminale a cui il processo è collegato.
2. `/sys/`: È uno pseudo file system che esporta, sullo spazio utente (attraverso file virtuali), informazioni su: vari sottosistemi del kernel, dispositivi hardware e driver di dispositivi associati dal kernel. Oltre a fornire informazioni su vari dispositivi e sottosistemi del kernel, i file virtuali esportati sono anche utilizzati per la loro configurazione.

<details>

```
The sysfs directory arrangement exposes the relationship of kernel
data structures. 

The top level sysfs directory looks like:

block/
bus/
class/
dev/
devices/
firmware/
net/
fs/

devices/ contains a filesystem representation of the device tree. It maps
directly to the internal kernel device tree, which is a hierarchy of
struct device. 

bus/ contains flat directory layout of the various bus types in the
kernel. Each bus's directory contains two subdirectories:

    devices/
    drivers/

devices/ contains symlinks for each device discovered in the system
that point to the device's directory under root/.

drivers/ contains a directory for each device driver that is loaded
for devices on that particular bus (this assumes that drivers do not
span multiple bus types).

fs/ contains a directory for some filesystems.  Currently each
filesystem wanting to export attributes must create its own hierarchy
below fs/ (see ./fuse.txt for an example).

dev/ contains two directories char/ and block/. Inside these two
directories there are symlinks named <major>:<minor>.  These symlinks
point to the sysfs directory for the given device.  /sys/dev provides a
quick way to lookup the sysfs interface for a device from the result of
a stat(2) operation.
```
</details>



## Tool generici

Ci sono diversi tool generici che risultano molto comodi quando si analizza il kernel linux. Molti sono normalmente utilizzati ma vengono inseriti per completezza.  
Tool utilizzati:

1. `dmesg`: Questo comando mostra tutti i messaggi dal buffer del kernel linux. Comodo per cercare tra i messaggi di boot.
   * È possibile limitare l'output a strutture e livelli (flag `-f`):
     * `kern` - messaggi del kernel
     * `user` - messaggi a livello di utente
     * `mail` - sistema di posta
     * `daemon` - demoni di sistema
     * `auth` - messaggi di sicurezza/autorizzazione
     * `syslog` - messaggi syslogd interni
     * `lpr` - sottosistema stampante di linea
     * `news` - sottosistema di notizie di rete
   * supporta i seguenti livelli di registro, che indicano l'importanza del messaggio (flag `-l`)
     * `emerg` - il sistema è inutilizzabile
     * `alert` - l'azione deve essere intrapresa immediatamente
     * `crit` - condizioni critiche
     * `err` - condizioni di errore
     * `warn` - condizioni di avvertimento
     * `notice` - condizioni normali ma significative
     * `info` - informativo
     * `debug` - messaggi a livello di debug
2. `grep`: Utilizzato per:
   * Filtro assieme ad altri comandi.
   * Ricercare una determinata stringa di caratteri in un file.
   * Espressioni regolarti `grep <regex> <file>`
   * Flag utili:
     * `-r`: recursive.
     * `-i`: grep di base e case sensistive e con questo flag disabilita questa opzione (molto utile).
3. `less / more`: Mostra un output permettendo di scorrere il contenuto. Molto comodo quando si aprono file di grandi dimensioni. Nota: `less` è più rapido perché non apre tutto il file in una volta sola.
4. `mount`: Permette di montare un fs o dispositivi rimovibili nel path specificato.
   * `-l`: Mostra la lista dei fs/dischi e dove sono montati
   *  Esempio di utilizzo `sudo mount -o rw,nodev /dev/sdb1 /media/usbdisk`
5. `df`: Utile comando per vedere l'utilizzo dei dischi montati.
   * `-a`: Mostra tuttu i file fs
   * `-T`: Mostra il tipo di file system utilizzato
   * `h`: Mosta i dati in human readable
6. `ps aux`: Mostra tutti i processi attivi.
7. `top / top`: Mostra i processi attivi in real time.
8. `ncdu`: Utile tool per vedere l'occupazione di memoria mostrata come tree navigabile
9. `tar`: Tool per compressione e decompessione di archivi
   1. Estrazione file `tar.gz`: `tar -xvf archive.tar.gz`
      1. `-v` Aggiunge verbosità
      2. Estrazione su specifica cartella: `tar -xvf archive.tar.gz -C /path/to/folder/`
      3. Estrazione file specifico: `tar -xvf archive.tar.gz file1 file2`
      4. Lista dei file compressi: `tar -tvf archive.tar.gz`
   2. Compressione file/cartelle: `tar -czvf name-of-archive.tar.gz /path/to/directory-or-file`
      1. `-c`: CRea archivio
      2. `-z`: Comprimo come gzip
      3. Si possono comprimere più file e cartelle in un colpo solo
      4. `--exclude`: esclude file e le cartelle specificate es. `tar -czvf archive.tar.gz /home/ubuntu --exclude=*.mp4`
   3. Compressione con `bzip2`
      1. Più lento ma comprime maggiormente (non tanto)
      2. `tar -cjvf archive.tar.bz2 stuff`
10. Tool per conversione valori di memoria:
    1. `echo 2G | numfmt --from=si`
    2. `echo 2G | numfmt --from=iec`
11. `fdisk`
    1. permette di formattare una memoria settare un file system
    2. Utile calcolo per calcolare la memoria da allocare:
       1. swap_bytes (memoria da allocare in GB): = 2 * 1024 * 1024 * 1024
       2. offset (opffset di memoria da cui partire)
       3. Il disco è solitamente diviso in blocchi da 521 byte (sector_size)
       4. sectors (settori necessari per avere quella memoria allocata): swap_bytes / sector_size
       5. last sector (da impostare): sectors + offset
12. `udevadm`:
    1. udev management tool, utile per avere info sui dispositivi gestiti da udev
    2. Utile esempio, verificare l'rtc: `udevadm info -a /dev/rtc0`
       1. Ritorna molte informazioni utili sul device e inoltre con `-a` verifica anche i parent drivers/busses superiori

## Device

I device riconosciuti dal sistema si trovano tutti in `/dev/`. Da linux è possibile interagire con i device scivendo nei registri (usando il comando `echo <val> > path/to/register`). Di seguito vengono mostrati alcuni device e i comandi relativi per la loro gestione.

### I/O device

Gli input device sono tutti quei device che interagiscono con Linux fornendo dei segnali di input/output.

#### GPIO

I pin GPIO permettono un interazione I/O con Linux e possono essere pilotati da sysfs. I pin sono configurati nel device tree seguendo le informazioni presenti nella userguide del microprocessore, e poi sono accessibili da user space per effettuare dei test nei singoli pin.  

I pin di GPIO si possono trovate in `/sys/class/gpio/`. vedendo il contenuto di questa cartella si troveranno di default due pseudo file:

* `export`
* `unexport`

Questi file permettono di esportare o rimuovere un pin di GPIO. Questa operazione può essere fatta o manualmente o direttamente dai driver (che pilotano la periferica) al boot di sistema. Quando si fa l'export di un pin questo diventerà disponibile da `sysfs` e potrà essere pilotato da Linux. Adesso si vedranno le operazioni per esportare e settare questi pin:

1. `$ echo <pin_number> > /sys/class/gpio/export`: Rende il pin specifico un pin GPIO
   * Con il comando `ls  /sys/class/gpio/` si vedrà nella cartella il file: gpio<pin_number>@ (es. `gpio24@`).
2. Si deve definire se il pin e in o out:
   1. `$ echo in >/sys/class/gpio/gpio<pin_number>/direction`.
   2. `$ echo out >/sys/class/gpio/gpio<pin_number>/direction`.
3. `$ echo <0/1> >/sys/class/gpio/gpio<pin_number>/value`: Setta il valore del pin. Nota che questa operazione è valida solo se il pin è settato come pin di output.
4.  `$ echo <pin_number> > /sys/class/gpio/unexport`: Toglie il pin specifico dagli export.
5.  Se il pin è stato esportato da un driver questo avrà un nome specifico definito all'interno del driver.

##### Configurazione pin GPIO a livello kernel/DT

###### NXP

Per fare il `PAD Multiplexing` da device tree bisogna:

1. Definire un device tree node

    <details>
    
    ```
    touch: egalax_i2c@2A {
        compatible = "eeti,egalax_allpoint_i2c";  // Definisce quale driver usare
        reg = <0x2A>;                             // Definisce il chip select number
        pinctrl-names = "default";
        pinctrl-0 = <&pinctrl_egalax_i2c_pins>;   // Dice quale pin control usare
        interrupt-parent = <&gpio5>;
        interrupts = <15 IRQ_TYPE_EDGE_FALLING>;
        int-gpios = <&gpio5 15 GPIO_ACTIVE_LOW>;  //Definisce il banco di gpio
        reset-gpio = <&gpio5 14 GPIO_ACTIVE_LOW>; // Definisce il il blocco gpio e l'offset
        status = "okay";                          // Abilita il nodo
    };
    
    ```
    </details>

2. Definire il `pinctrl` ovvero come configurare il pin specifico, definendo la sua funzione.

    <details>
    
    ```
    egalax_i2c {
      pinctrl_egalax_i2c_pins: edt-1 {
        fsl,pins = <
          MX6QDL_PAD_DISP0_DAT20__GPIO5_IO14  0x4001b0b1 /* rst vdd */
          MX6QDL_PAD_DISP0_DAT21__GPIO5_IO15  0x4001b0b1 /* int */
        >;
      };
    };
    ```
    
    </details>

   * Analisi della nomenclatura:
     * ` MX6QDL_PAD`: Indica pad che si ha dipendentemente dal micro (?)
     * `DISP0_DAT20` Indica il PAD/PIN name dell'IC
     * `GPIO5_IO14`: Indica la funzionalità che si vuole assegnare al pad


#### Touchscreen

Questo dispositivo si trova in `/dev/input/`.  
Comandi utili per il toucscreen:

1. `ls -la /dev/input/`: Mostra tutti gli input e mostra il link all'event utilizzato. Un esempio:

    <details>
    
    ```
    root@imx6qxelk:~# ls -la /dev/input/
    total 0
    drwxr-xr-x  2 root root     160 Jan  1 00:00 .
    drwxr-xr-x 12 root root    3980 Jan  1 00:00 ..
    crw-rw----  1 root input 13, 64 Jan  1 00:00 event0
    crw-rw----  1 root input 13, 65 Jan  1 00:00 event1
    crw-rw----  1 root input 13, 63 Jan  1 00:00 mice
    crw-rw----  1 root input 13, 32 Jan  1 00:00 mouse0
    crw-rw----  1 root input 13, 33 Jan  1 00:00 mouse1
    lrwxrwxrwx  1 root root       6 Jan  1 00:00 touchscreen0 -> event0
    ```
    
    </details>

2. `evtest /dev/input/<event>`: test che mostra le coordinate dei punti toccati sul touchscreen.

    <details>

    ```
    root@imx6qxelk:~# evtest /dev/input/event0
    Input driver version is 1.0.1
    Input device ID: bus 0x18 vendor 0xeef product 0x20 version 0x1
    Input device name: "eGalax_Touch_ScreenAllPoint"
    Supported events:
      Event type 0 (EV_SYN)
      Event type 1 (EV_KEY)
        Event code 330 (BTN_TOUCH)
      Event type 3 (EV_ABS)
        Event code 0 (ABS_X)
          Value      0
          Min        0
          Max     4095
        Event code 1 (ABS_Y)
          Value      0
          Min        0
          Max     4095
        Event code 47 (ABS_MT_SLOT)
          Value      0
          Min        0
          Max       15
        Event code 48 (ABS_MT_TOUCH_MAJOR)
          Value      0
          Min        0
          Max      255
        Event code 53 (ABS_MT_POSITION_X)
          Value      0
          Min        0
          Max     4095
        Event code 54 (ABS_MT_POSITION_Y)
          Value      0
          Min        0
          Max     4095
        Event code 57 (ABS_MT_TRACKING_ID)
          Value      0
          Min        0
          Max    65535
    Properties:
      Property type 1 (INPUT_PROP_DIRECT)
    Testing ... (interrupt to exit)
    ```

    </details>

#### Display

I display possono essere controllati attraverso diverse operazioni:

1. Modificare la backlight: `echo <value [0-255]> > /sys/class/backlight/backlight/brightness`

## Periferiche

Gestione delle varie periferiche. Di seguito una lista.

### I2C

Comandi utili:

1. `i2cdetect -y <i2cbus>`: Mostra il chip address delle periferiche collegate al bus I2C specificato -> il valore UU indica che è stato fotto il binding  (es. `i2cdetect -y 2`)

    <details>

    ```
    root@imx6qxelk:~# i2cdetect -y 2
         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:          -- -- -- -- -- -- -- -- -- -- -- -- --
    10: -- -- -- -- -- -- -- -- UU -- -- -- -- -- -- --
    20: -- -- -- -- -- -- -- -- -- -- UU -- -- -- -- --
    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    40: UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    50: -- UU -- -- -- -- -- -- -- -- -- -- -- -- -- --
    60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    70: -- -- -- -- -- -- -- --
    ```

    </details>

1. `i2cget -y <i2cbus> <chip-address> <data-address [mode]>`: Legge il registro specificato dalla periferica I2C (es. `i2cget -y 2 0x2a 0x00`)
2. `i2cdump -y <i2cbus> <chip-address>`: Esegue il dump di tutti i registri della periferica specificata (es. `i2cdump -y 2 0x2a`)
3. `echo <i2cbus>-<chip-address> > /sys/bus/i2c/drivers/egalax_allpoint_i2c/bind`: Fa il binding della periferica specificata (es. `echo 2-002a > /sys/bus/i2c/drivers/egalax_allpoint_i2c/bind`)
4. `echo <i2cbus>-<chip-address> > /sys/bus/i2c/drivers/egalax_allpoint_i2c/unbind`:  Fa l'unbinding della periferica specificata (es. `echo 2-002a > /sys/bus/i2c/drivers/egalax_allpoint_i2c/unbind`)

## ETH

L'ethernet può essere controllato a livello Linux e uboot:

* u-boot:
  * Da u-boot si può gestire il phy tramite i tool
    * `mii`
    * `mdio`
    * Con questi tool è possibile leggere/scrivere sui registri del phy per impostare le configurazioni volute
* Linux:
  * tramite il tool `ethtool`
  * Permette di configurare velocità di trasmissione
  * Controllare l'autonegoziazione
  * E qualsiasi tipo di operazione sul PHY eth, vedere `ethtool -h`