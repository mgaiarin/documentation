# LVDS

Durante l'integrazione dei display LVDS nel DT è necessario impostare correttamente vari parametri, tra questi:

* Timing power sequence
* Timing LVDS
* Assegnare correttamente i gpio per il pilotaggio di
  * livelli di brightness
  * Power
  * BL
  * PWM

## Power sequence

Esempio di una power sequence

```typescript
backlight {
    pinctrl-names = "default";
    pinctrl-0 = <&pinctrl_display_lvds_salcr>;
    compatible = "pwm-backlight";
    pwms = <&pwm1 0 100000>;
    brightness-levels = <0 3 6 8 11 13 16 18 21 23 26 29 31 34 36 39 41 44 46 49 51 54 57 59 62 64 67 69 72 74 77 80 82 85 87 90 92 95 97 100 102 105 108 110 113 115 118 120 123 125 128 131 133 136 138 141 143 146 148 151 153 156 159 161 164 166 169 171 174 176 179 182 184 187 189 192 194 197 199 202 204 207 210 212 215 217 220 222 225 227 230 233 235 238 240 243 245 248 250 253 255>;
    default-brightness-level = <75>;

    power-sequence = "power", "lvds", "bl", "pwm", "0";
    power-on-after = <500000>;

    /* Gpio order as power-sequence. Do not specify gpio pin for pwm */
    /* power */
    power-seq-gpio1 = <&gpio1 2 GPIO_ACTIVE_HIGH>;
    power-seq-timing-on1 = <25000>;
    power-seq-timing-off1 = <25000>;

    /* lvds */
    power-seq-timing-on2 = <150000>;
    power-seq-timing-off2 = <50000>;

    /* bl */
    power-seq-gpio3 = <&gpio7 12 GPIO_ACTIVE_HIGH>;
    power-seq-timing-on3 = <10000>;
    power-seq-timing-off3 = <10000>;

    /* pwm */
    power-seq-timing-on4 = <0>;
    power-seq-timing-off4 = <0>;
};

```

Cosa troviamo:

* brightness-levels: Livelli PWM per impostare la luminosità. Il valore va da 0 a 255 e ogni valore identifica una percentuale PWM

```typescript
Array of distinct brightness levels. Typically these
are in the range from 0 to 255, but any range starting at 0 will do.
The actual brightness level (PWM duty cycle) will be interpolated
from these values. 0 means a 0% duty cycle (darkest/off), while the
last value in the array represents a 100% duty cycle (brightest).
```

* Power Sequence: ref driver `backlight.c`
  * power-sequence: Lista dei componenti da pilotare
  * power-seq-gpioX: Indica il gpio da pilotare
  * power-seq-timing-on1: Valore in ns del delay dopo cui andare ad accendere la periferica
  * power-seq-timing-off1: Valore in ns del delay dopo cui andare ad spegnere la periferica
  * 