# Petalinux

Petalinux si basa su Yocto ma ha un layer sw aggiuntivo che semplifica il su utilizzo, mascherando varie informazioni.  
Si riportano utili comandi per il suo utilizzo, fare sempre riferimento alla guida uffciale

## Setup

Il setup inziale per eseguire le build prevede:

* Setup uSD
  * Preparare un SD con 2 partizioni:
    * the first must be an FAT32 partition (type b) with size of at least 64MB
    * the second is a normal linux partition (type 83) of at least 512M with ext3/ext4 filesystem
* Accedere alla cartela di lavoro `cd petalinux`
* Sourcing settings: `source /opt/Xilinx/petalinux/2021.2/settings.sh`
* Inclusione del hw description nel codice: `petalinux-config --get-hw-description ../vivado/vivado/bora.xsa`
  * Questo permette di ottenere un DT già pre compilato da petalinux
* Cofigurazioni di sistema:
  * `petalinux-config -c kernel`
    * Permette di accedere alla kconfig del kernel e aggiungere/rimuovere moduli
  * `petalinux-config -c u-boot`
    * Permette di accedere alla kconfig di u-boot e aggiungere/rimuovere moduli

## Build

La build si può eseguire tramite vari comandi in base alle esigenze:

* Build completa: `petalinux-build`
* Build kernel: `petalinux-build -c kernel`
* Build DT: `petalinux-build -c device-tree`
* Generare i binari:
  * `petalinux-package --boot --u-boot --force`
  * Questo comando permette di generare i binari necessari al boot tramite `FSBL`
  * Ulteriori flag permettono di aggiungere specifici binari ai file di boot
    * `cp images/linux/BOOT.BIN </path to SD FAT32 partition>`
    * `cp images/linux/image.ub </path to SD FAT32 partition>`
    * `cp images/linux/boot.scr </path to SD FAT32 partition>`
    * `sudo tar -xpf images/linux/rootfs.tar.gz -C </path to SD ext3 partition>`