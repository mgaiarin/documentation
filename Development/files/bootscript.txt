echo 'bootscript generated with command "mkimage -A ARM -T script -C none -n DESK-MX6UL -d bootscript.txt boot.scr"'

setenv desk_release 'desk-mx6ul-l-3.0.0'

if test 0x${som_configid#} = 0x00000013 && test 0x${cb_configid#} = 0x0000002f;
then
   setenv fdtfile ${desk_release}_imx6ul-lynx-som0013-cb002f.dtb
elif test 0x${cb_configid#} = 0x0000003a
then
   setenv fdtfile ${desk_release}_imx6ul-axelulite-cb003a.dtb
elif test 0x${cb_configid#} = 0x0000006c
then
   setenv fdtfile ${desk_release}_imx6ul-axelulite-cb006c.dtb
else
   echo Invalid CB! Autoreset ...
   sleep 30
   reset
fi

setenv bootfile ${desk_release}_uImage

setenv mmc_loadk 'fatload mmc ${mmcdev}:1 ${loadaddr} ${bootfile}'
setenv mmc_loadfdt 'fatload mmc ${mmcdev}:1 ${fdtaddr} ${fdtfile}'

echo Booting DESK-MX6UL via mmcboot with ${fdtfile} as device tree

run mmcboot

echo mmcboot FAILURE