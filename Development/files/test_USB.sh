#!/bin/bash

# Funzione di help
show_help() {
    echo "Uso: $0 [opzione] <device>"
    echo
    echo "Testa la velocità di lettura e scrittura di un dispositivo USB."
    echo
    echo "Opzioni:"
    echo "  -h, --help      Mostra questo messaggio di aiuto"
    echo
    echo "Argomenti:"
    echo "  <device>        Il dispositivo USB da testare (esempio: /dev/sda1)"
    echo
    echo "Lo script esegue i seguenti passaggi:"
    echo "  1. Monta il dispositivo in una directory temporanea"
    echo "  2. Scrive un file di test casuale di 100 MB"
    echo "  3. Legge il file di test, misurando velocità di scrittura e lettura"
    echo "  4. Rimuove il file di test e smonta il dispositivo"
    echo
    echo "Esempio:"
    echo "  sudo $0 /dev/sda1"
    echo
}

# Verifica opzioni o argomenti
if [ "$#" -lt 1 ]; then
    echo "Errore: nessun dispositivo specificato."
    echo "Usa '$0 --help' per maggiori informazioni."
    exit 1
fi

if [[ "$1" == "-h" || "$1" == "--help" ]]; then
    show_help
    exit 0
fi

DEVICE=$1
MOUNT_DIR="/mnt/test_usb"
TEST_FILE="test_file.bin"
FILE_SIZE=100M  # Dimensione del file di test

# Funzione per pulire e uscire in caso di errore
cleanup() {
    echo "Pulizia in corso..."
    if mountpoint -q "$MOUNT_DIR"; then
        umount "$MOUNT_DIR"
    fi
    rm -rf "$MOUNT_DIR"
    exit 1
}

# Creazione della directory di mount
echo "Creazione directory di mount..."
mkdir -p "$MOUNT_DIR" || { echo "Errore nella creazione della directory $MOUNT_DIR"; exit 1; }

# Monta il dispositivo
echo "Montaggio di $DEVICE su $MOUNT_DIR..."
mount "$DEVICE" "$MOUNT_DIR" || { echo "Errore nel montaggio di $DEVICE"; cleanup; }

# Scrittura del file di test con misurazione della velocità
echo "Scrittura del file di test ($FILE_SIZE)..."
WRITE_SPEED=$(dd if=/dev/urandom of="$MOUNT_DIR/$TEST_FILE" bs=1M count=100 conv=fsync 2>&1 | grep -Eo '[0-9.]+ MB/s')

if [ $? -ne 0 ]; then
    echo "Errore nella scrittura del file di test"
    cleanup
fi

echo "Velocità di scrittura: $WRITE_SPEED"

# Droppare la cache
echo "Dropping cache per test di lettura..."
sync
echo 3 > /proc/sys/vm/drop_caches

# Verifica della lettura del file di test con misurazione della velocità
echo "Lettura del file di test..."
READ_SPEED=$(dd if="$MOUNT_DIR/$TEST_FILE" of=/dev/null bs=1M 2>&1 | grep -Eo '[0-9.]+ MB/s')

if [ $? -ne 0 ]; then
    echo "Errore nella lettura del file di test"
    cleanup
fi

echo "Velocità di lettura: $READ_SPEED"

# Pulizia
echo "Rimozione del file di test..."
rm "$MOUNT_DIR/$TEST_FILE"
umount "$MOUNT_DIR" || { echo "Errore nello smontaggio di $DEVICE"; exit 1; }
rm -rf "$MOUNT_DIR"

echo "Test completato con successo."
exit 0
