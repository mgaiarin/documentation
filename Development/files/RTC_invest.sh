#!/bin/bash

LOG="./test.log"
rm -f "$LOG"

# Funzione per mostrare l'help
function show_help() {
    echo "RTC Drift Test Script"
    echo
    echo "Uso:"
    echo "  $0 [tempo_in_secondi] [rtc] [intervallo]"
    echo
    echo "Parametri:"
    echo "  tempo_in_secondi   Specifica la durata del test in secondi (es. 60 per 1 minuto)."
    echo "                     Se omesso, il test durerà 24 ore (86400 secondi)."
    echo "  rtc                Nome del dispositivo RTC da testare (es. rtc0, rtc1)."
    echo "                     Se omesso, il valore predefinito è 'rtc0'."
    echo "  intervallo         Intervallo tra i controlli in secondi (default: 60)."
    echo
    echo "Opzioni:"
    echo "  --help             Mostra questa guida e termina."
    echo
    echo "Descrizione:"
    echo "  Questo script sincronizza l'orologio di sistema (sysclock) e l'orologio hardware (RTC)"
    echo "  con un server NTP. Durante il test, registra le discrepanze tra i due orologi per valutare il drift."
    echo
    echo "Esempi:"
    echo "  $0 60 rtc1 10      Esegue il test per 1 minuto usando rtc1 con intervalli di 10 secondi."
    echo "  $0                 Esegue il test per 24 ore usando rtc0 (default) con intervalli di 60 secondi."
    echo
    exit 0
}

# Controllo se è stato richiesto l'help
if [ "$1" == "--help" ]; then
    show_help
fi

{
SERVERIP="192.168.0.23"

# Parametro RTC (default: rtc0)
RTC=${2:-rtc0}
if [ -z "$2" ]; then
    echo "RTC non specificato, uso il valore predefinito: $RTC."
else
    echo "Utilizzo RTC specificato: $RTC."
fi

PATH_RTC="/dev/$RTC"

# Parametro intervallo (default: 60 secondi)
INTERVAL=${3:-60}
if ! [[ "$INTERVAL" =~ ^[0-9]+$ ]] || [ "$INTERVAL" -le 0 ]; then
    echo "Error: Intervallo non valido. Deve essere un numero intero positivo."
    exit 1
fi
echo "Intervallo tra i controlli: ${INTERVAL} secondi."

echo
echo "**** RTC CLOCK TEST ****"
echo
echo "TEST on RTC (check this out):"
if ! [ -e "/sys/class/rtc/$RTC/name" ]; then
    echo "Error: RTC device $RTC does not exist!"
    exit 1
fi
cat "/sys/class/rtc/$RTC/name"

# Check input validity
if [ -z "$1" ]; then
    echo
    echo "Settings default running time to 24h..."
    DELTA=86400
elif [[ "$1" =~ ^[0-9]+$ ]]; then
    echo
    echo "Settings running time to ${1} seconds..."
    DELTA="$1"
else
    echo
    echo "Error: Input is not a valid positive number."
    exit 1
fi
end=$((SECONDS + DELTA))

# Stop NTP services if running
for service in ntpdate systemd-timesyncd; do
    STATUS=$(systemctl is-active "$service")
    if [ "$STATUS" = "active" ]; then
        echo
        echo "Stopping $service service..."
        systemctl stop "$service"
    else
        echo
        echo "$service is already stopped."
    fi
    systemctl status "$service" | head -n 3
done

# Drop adjtime if it exists
if [ -f /etc/adjtime ]; then
    echo "Dropping /etc/adjtime..."
    rm /etc/adjtime
fi

echo
echo "Syncing time with server ${SERVERIP}..."
if ! ntpdate "$SERVERIP"; then
    echo "Error: Unable to sync time with server $SERVERIP."
    exit 1
fi

echo
echo "Copying sysclock to hwclock..."
hwclock -f "$PATH_RTC" --systohc --noadjfile --utc
hwclock -f "$PATH_RTC" -r

# Variabile per drift finale
final_drift=0

echo
echo "Running test..."
while [ "$SECONDS" -le "$end" ]; do
    echo
    echo "Checking NTP and RTC clock:"
    ntpdate_output=$(ntpdate -q "$SERVERIP")
    echo "$ntpdate_output"
    hwclock -f "$PATH_RTC" -r
    sleep "$INTERVAL"
done

# Sincronizzazione finale
echo
echo "Final sync from hwclock to sysclock..."
hwclock -f "$PATH_RTC" --hctosys --noadjfile --utc
hwclock -f "$PATH_RTC" -r

echo
echo "Checking final drift with NTP server:"
final_ntp_output=$(ntpdate -q "$SERVERIP")
echo "$final_ntp_output"

# Calcolo del drift
final_drift=$(echo "$final_ntp_output" | awk '/offset/ {print $10}')
echo
echo "**** DRIFT REPORT ****"
echo "Drift finale rispetto al server NTP: ${final_drift} secondi."

} | tee "$LOG" 2>&1
