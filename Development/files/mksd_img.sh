#!/bin/bash

if [[ -z $1 || -z $2 || -z $3 || -z $4 || -z $5 ]]
then
   echo "$0 Usage:"
   echo "	$0 <device> <u-boot.img> <SPL> <binaries directory> <rootfs tar.bz2>"
   echo "	Example: $0 /dev/sdc u-boot.img SPL binaries/ rootfs.tar.bz2"
   exit
fi

if [ "$(whoami)" != "root" ]
then
   echo "you must be root to run this script!"
   exit
fi

if ! [[ -b $1 ]]
then
   echo "$1 is not a valid block device!"
   exit
fi

if ! [[ -e $2 ]]
then
   echo "Incorrect u-boot.img location!"
   exit
fi

if ! [[ -e $3 ]]
then
   echo "Incorrect SPL location!"
   exit
fi

if ! [[ -d $4 ]]
then
   echo "Incorrect Binaries location!"
   exit
fi

if ! [[ -f $5 ]]
then
   echo "Incorrect rootfs location!"
   exit
fi

DRIVE=$1
if [[ "$DRIVE" == *"mmcblk"* ]]
then
   echo "You're using a mmc device, I need to fix partition names"
   PART="p"
else
   PART=""
fi
UBOOT=$2
SPL=$3
BINARIES=$4
RFS=$5

echo "All data on "$DRIVE" now will be destroyed! Continue? [y/n]"
read ans
if ! [ $ans == 'y' ]
then
   exit
fi

echo "[Partitioning $1...]"

dd if=/dev/zero of=$DRIVE bs=1024 count=1024

SIZE=`fdisk -l $DRIVE | grep Disk | awk '{print $5}'`

echo DISK SIZE - $SIZE bytes

CYLINDERS=`echo $SIZE/255/63/512 | bc`

# check if we're running an old (e.g. 2.20.x) or new (e.g. 2.24.x) sfdisk
sfdisk --help | grep -- -H

if [ "$?" -eq "0" ]
then
   {
      echo 40,1380,0x0c,*
      echo 1420,,83,-
   } | sfdisk -D -H 255 -S 63 -C $CYLINDERS $DRIVE
else
{
   echo 16M,8176M,0x0c,*
   echo 8192M,,83,-
} | sfdisk $DRIVE
fi

partprobe


echo "[Making filesystems...]"
mkfs.vfat -F 32 -n BOOT "$DRIVE$PART"1 #> /dev/null
mkfs.ext3 -F -L ROOTFS "$DRIVE$PART"2 #> /dev/null

echo "[Copying files...]"

binaries_dir=${BINARIES%/}
mount "$DRIVE$PART"1 /mnt
cp -av --no-preserve=ownership $binaries_dir/* /mnt/
umount "$DRIVE$PART"1

echo "[Extracting rfs (this may take a while...)]"
mount "$DRIVE$PART"2 /mnt
tar jxf $RFS -C /mnt > /dev/null
chmod 755 /mnt
umount "$DRIVE$PART"2

echo "[Programming SPL]"
dd if=$SPL of=$DRIVE bs=512 seek=2 conv=fsync

echo "[Programming u-boot.img]"
dd if=$UBOOT of=$DRIVE bs=1k seek=69 conv=fsync

echo "[Done]"