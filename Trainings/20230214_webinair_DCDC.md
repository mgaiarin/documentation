# DCDC design

## Definizioni delle T

* Junction Temp
  * Tensione di giunzione che raggiunge le T più alte
  * Absolute maximum junction T 
    * Tensione di massima T oltre la quale si rompe il componente
  * Max operating T
    * Massima T di lavoro ma con specifice non risp sicuramenete
  * Recommended
    * T consigliata per funzionare correttamente
  * Ambient Air T
    * Temperatura ambientale consigliata a cui si può lavorare
    * Si considerano le T del PCB
    * Se Tamb 125 sul PCB si avrà 135 -> Devo considerare questa

## Thermal metrics

* Theta: indica il package del componente
  * Serve al dimensionamento termico
  * Utile in fase di progettazione
  * Theta_JA 
    * Dipende da molti fattori
      * Dal comp stesso
        * Es. dim package
      * rame sul PCB
      * stackup del PCB
* Psi:
  * Dato di caratterizzazione termica introdotto da poco
  * Dato che serve a capire la temperatura della giunzione misurando la T del componente
  * Utile in fase di testing termico

## Therma resistance

Formula T_j = T_c + ( R_thetaJC x Power)

Errori da non fare:

* Considera T_amb non quella del PCB ma dell'ambiente
* Considerare la Theta_JA del datasheet, ma solitamente questa è più bassa. 
  * Bisogna considerare anche il resto dei componenti montati
  * Va quindi considerato un margine dato anche dal progetto

Si veda di seguito come si dissipa la T sul package. Come si vedrà la psi permette di calcolare meglio la T_j.

### Thermal PAD for Exposed Pad Package

* Compoenenti che hanno il pad sul package possono dissipare sul top e sul bottom tramite dei via.
  * ~80% della dissipazione avviene su top e bottom del PCB
  * I layer interni aiutano poco
  * I pin esterni dissipano ~20%
  * Sul package si dissipa un ~2%

### Thermal PAD for HotRod

* Utili perché tolgono la connessione tra pin e die
  * Riduce la C parassita
  * Quasi tutta la dissipazione avviene sulla PAD
  * ~1% sul package

### PSI

Il Junction to top dipende:

* Layout
* Stackup PCB

La misura viene fatta [Rivedere]

## Calcolo I_out_max

Dipende da molti fattori:

* T_J
* T_A
* R_theta_JA
  * Valore critico, dipende da:
    * PCB Area
    * Copper nell'area
    * Numero di via
    * Flusso d'aria
    * Componenti vicini

## Stima di power dissipation

* Nel calcolo della P dissipata dal componente il dato di efficienza da un dato complessivo
  * considera anche le perdite nell'induttanza.
* Bisogna togliere la P dissipata dai comp estreni, es, induttore

## Estimate PCB Area

* Ci sono dei tool TI online che aiutano a calcolare la dissipazione termica.
* Ci sono anche file Excel con vari componenti con i calcolatori per valutare la dissipazione
* Una buona cosa è quella di fare un approssimazione della minima area di rame da usare dati un Theta_JA e Thetha_JC.
  * Ci sono formula empiriche su questo

Copper AREA: Area specifica di rame sotto il componente e non di tutta la scheda (non facile da stabilire)

## PCB Copper Thickness

Per potenze superiori ai 2W e meglio usare 2oz di rame di spessore, migliora la dissipazione anche di un ~20/25%

## Calcoli vari

### massima Theta_JA

Conoscendo:

* T_Jmax


### Copper Area

Conoscendo:

## Ottimizzazioni

### PCB Copper layer

Maggior parte della dissiazione su TOP e BOTTOM. Layer interni aiutano ma non sono primari
Meglio usare via termici per portate T sul BOTTOM

### Optimum Cuts

Ciò che va bene per la EMC e male per la dissipazione termica.
Avere compoentni distanti migliora la dissipazione.

### Hotspot

Ridurre punti di hot spot dove ci sono molti comp che dissipano

### Via

Il via dovrebbe essere almeno di 12 mils per dissipare abbastanza bene. Ha un res termica di ~100°/W quindi bisogna massimizzare il numero di via.
Se si esagera però si possono creare delle strozzature termiche.
