# LAUTERBACH

## Introduzione all'HW Lauterbach

* Modulo universale (cassone)
  * Ogetto indipendente dall'architettura della CPU
  * Si collega al PC tramite USB 3.0
  * Modelli pro forniscono connettività di rete
  * Notare la serigrafia per la giusta connessione
* Debugger
  * È la parte mobile del lauterabach
  * Questo oggetto va selezionato per lo specifico HW con cui lavorare
  * Ha un pinout standard 20
  * La connettività si può verificare sul sito in base al debugger utilizzato
* Il sistema non è plug&play -> Collegare il DUT spento
  * Serve un riferimento di tensione corretto per inzializzare il sistema
* Pinout
  * ref. app_arm_jtag.pdf disponibile sulla cartella di intallazione
  * RESET: Utile per resettare i core. Permette di partire da un sistema in condizioni note.
    * Il segnale è opzionale ma può risultare molto utile per evitare reset HW
    * Il reset può essere I/O è permette di verificare il livello di tensione e se avviene un reset
    * Può forzare un reset SW.
      * Noi non lo usiamo solitamente perchè il reset nostro forza un POR e quindi viene rimossa completamente l'alimentazione
  * GND: Presenti molte linee di GND per avere dei segnali più puliti
* refdebug:
  * Capisce se il sistema è abilitato
  * Utilizzato anche per definire il livello dei segnali
    * Se si hanno più domini di alimentazione collegare il dominio per il debug

### Installazione e configurazione

* L'installazione crea vari eseguibili in base alla piattaforma utilizzata (es. t32arm.exe)
  * t32start.exe:
    * GUI presente solo in windows per creare/modificare un file di configurazione
    * Il file creato è una sorta di patch che viene applicata al file di conf principale
* Presente un il file config_t32.
  * Il file ha delle configurazioni preparate durante l'installazione.
  * Molte conf sono opzionali
  * conf tipiche:
    * OS
    * PDI:
      * Identifica la periferica da utilizzare per il debug
      * Si può definire anche un simulatore: Utile anche per caricare dump di sistema salvati precedentemente.
    * printer
    * remotecontrol
    * screen
    * license
* Presente un workingdirectoy dove saranno presenti tutti i file (script, ecc.)
  * Questa cartella deve stare fuori dalla cartella di installazione per evitare di perdere informazioni
* Licenza
  * Presenti due licenze
    * feature
      * Ogni licenza è associata al debugger specifico e non scadono.
      * Sono associate ad uno specifico SW, non si possono uusare versioni più aggiornate.
      * La licenza si può associare ad una famiglia HW (es. ARM)
      * Si possono avere installazioni diverse del T32
        * Ogni installazione deve avere uno specifico file di configurazione, che deve puntare alla system directory su cui si sta lavorando.
        * Se si fa un aggiornamento basta copiare la cartella prima di iniziare (l'aggiornamento è uno zip che viene scompattato).
    * maintenace
      * Permette di aggiornare la licenza annualmente per usare versioni sempre nuove del T32

### Setup

* Comandi da GUI o da commandline
  * Ogni operazione può essere eseguita da in entrambi i modi
  * La barra inferiore fornisce supporto per comporre i comandi
  * La creazione di uno script può essere fatta guardando anche la GUI, risulta molto più semplice
* Script
  * Si può generare uno script con l'editor T32 che aiuta con l'autocompletamento
  * Lo script permette di salvare le impostazioni generiche per configurare il sistema
  * Solitamente conviene partire dagli script template del DUT
* Configurazione di base per collegarsi al sistema. Si riportano i comandi tipici:
  * `SYStem.RESet`: forza un reset della board.
  * `SYStem.state`: Apre l'interfaccia grafica
    * Qui si deve configurare il Core utilizzato dal sistema
      * Questa configurazione non è sufficiente perchè il T32 tenterà di collegarsi a tutti i core. Invece deve collegarsi solo al core di boot
    * `SYStem.CPU`: Comando per configurare la CPU da utilizzare
  * `CORE.ASSIGN 1` per indicare di collegarsi solo al core di boot
* Comandi utili:
  * `we.PLIST`: Finestra che mi dice lo stato di uno script
  * `wr.PEDIT`: Finestra per l'editing di uno script
  * `PSTEP`: Durante il debug di uno script permette di andare avanti di uno step
  * In uno script possono essere usati comandi o funzioni, le quali ritornano dei valori
  * `LIST`: Mostra il PC reale della board
* Help
  * È posssibile configurare il PDF viewer da usare per leggere la documentazione
    * Si può configurare il viewer da utilizzare
    * Si può configurare anche uno specifico capitolo da aprire
  * Per cercare la guida di un comando: inserire il comando nel barra e poi premere `F1`
  * Presente anche il comando index che permette di cercare i file di help su un tree

## High Level Language Debugging

Si vedano un po' di comandi per il debugging di una board:

* Per collegare un debug con la scheda attiva è necessario impostarlo in modalità `NoDebug`. In questo modo il debugger si mette in modalità tristate e va in alta impedenza, poi si può collegare il DUT e fare un `attach`.
* Si può vedere anche un dump della memoria, comando: `Data.dump <indirizzo>`
* `list`: riporta il codice che si sta eseguendo (se sono presenti i sorgenti mostra anche i simboli)
* `register`: Apre la finestra con i registri di sistema
  * `SPOTLIGHT`: Comodo flag  per evidenziare i registri che vengono modificati durante il debug -> più il colore 'sbianca' più tempo è passato da quando il registro è stato modificato
* Registri e memoria hanno delle classi di accesso specifiche che possono essere interpretati guardando la guida
  * es. `print.data.log(D:<indirizzo>)`
    * La classe in questo caso è la `D` che indetifica un offset qualificato. In questo caso un generico accesso di lettura in memoria.
    * Solitamento `D:` può andare bene
  * Una comoda classe è la `E:` la quale abilita l'accesso alla memoria in runtime.
    * Senza questa opzione durante il runtime il dump non può essere visto ed è richiesto un break
    * Questa lettura non è intrusiva, il core gira al 100%. Il debug accede alla memoria senza passare per la CPU ma per un bus dedicato.
    * Questi accessi sono comunque in cocorrenza con la CPU, ma l'arbitraggio viene gestito dalla CPU. Il core ha comunque la priorità di accesso alle risorse di memoria, cosa che garantisce ARM.
    * Va sempre considerato capire se il test che si sta eseguendo disturba il sistema stesso durante il debug.
    * Viene riportato il tempo di refresh dei registri, si può vedere che il debug comunque accede molto meno alle risorse rispetto a quato f ail core.
* Periferiche
  * Si possono vedere anche i registri delle periferiche con il comando `PER . /Dualport /spotlight`
  * USEFUL: In questo caso se sono presenti registri read and clear, la lettura di debug potrebbe cancellare il registro e modificare così il comportamento.
    * Spesso però i registri sono protetti e non si può vederne il valore
* Accessi in memoria
  * Cache non accessibile
  * MMU: anche questa non è accessibile
  * Si può vedere solo la memoria fisica
* Salvataggio dump di memoria
  * Risulta comodo poter salvare su file il dump di memoria/registri. In questo modo si possono fare verifiche sulle modifiche dei valori di memoria.
    * Questo salvataggio avviene as-is è riporta le informazioni del dump.
  * Si può fare una copia anche su clipboard e copiare su un file a mano.
    * In questo caso la visualizzazione è diversa, viene salvato in una versione scriptabile che poi può essere ricaricato.
    * Ovviamente ci sono limitazione, es. registri readonly che non possono essere impostati tramite lo script
  * Il tutto può essere scriptato in modo da posizionare dei break e salvare i dump di una locazione di memoria specifica
    * La selezione dell'area specifica può essere raggiunta sfruttando la toolbar in alto (TBD: da rivedere come selezionare un area specifica di memoria)
* Il debug che solitamente viene fatto si basa su SW già installato
  * In questo caso la board caricata già del FW e inizializza delle periferiche
  * Per un bring-up il tutto è più complicato perché il T32 ferma l'esecuzione il prima possibile e il debugger deve inizializzare tutto (o quasi)
* Caricamento simboli:
  * `DATA.Load.elf <path> /NoLoad`: Comando utile per caricare i simboli senza riscrivere la memoria.
    * In questo caso u-boot ha già caricato la memoria, si vuole quindi solo associare alla memoria i simboli ad alto livello (flag `/NoLoad` fa questo).
    * Ovviamente il FW caricato e i simboli usati devo essere allineati.
  * Una soluzione utile per il debug dalle prime istruzioni è quella di mettere un loop alla prima istruzione e poi spostare il PC manualmente.
  * `system.symbol.session`: Fornisce informazioni sui simboli che sono stati caricati.
  * `Symbol.List.MAP`: mostra informazioni su quello che ha fatto il comando di caricamento dell'elf
  * Altri flag:
    * `NoRegister`: permette di caricare i simboli senza spostare il PC
* Comandi `MMU`
  * `MMU.List.Page.Table`: Mostra la lista della MMU translation table
* RESET:
  * `SYStem.Option.WaitDCODE 1.5` Il bootrom tiene chiusa la cella debug. Solo dopo che questa viene attivata il debugger potrà collegarsi, ma prima può eseguire altre operazioni, come verifiche sui pin di alimentazione.
    * Questa complessità nella fase iniziale il debugger non può fare molto, il comando mostrato quindi serve a fare un polling per 1.5s e verificare se il chip risponde. Questa operazione serve se il siliconaro non da tempistiche più precise.
* Layout finestre
  * Si può salvare il layout delle finestre in uno script che le riposiziona come voluto.
  * Usare la funzione `store window`
    * Da command line `STore <name.cmm> win`

## Debug

* Si può verificare lo stack con il comando `view -> stackframe`
  * Sulla vista register si può ottenere la stessa vista
    * Solitamente questo non è espanso perchè potrebbe portare degli errori
    * La vista dello stack rende il tutto più veloce
  * Dalla vista frame è possibile spostarsi tra lo stack, mostrando lo stato macchina, con i tasti `up` e `down`
* Dalla vista dello stack è possibile muoversi tra i sorgenti e vedere step by step tutti i punti in cui lo stack si trova.
* `view -> symbol -> symbol tree view`: utile comando per identificare in modo preciso le variabili presenti nei sorgenti. Vengono date informazioni anche sui range di esistenza del PC delle variabili
  * vengono date info di quando la variabile ha un valore sensato solo se il PC è nello specifico range in cui la variabile ha senso.
  * Se una funzione chiama un altra funzione si hanno due casi:
    * Il valore della variabile rimane allocata nello stack -> il valore e nel campo di esistenza
    * Il valore della variabile non è nello stack -> il valore NON è più nel capo di esistenza. In questo caso la variabile mostra come valore dei `?` che indicano che il suo valore non è sensato perchè il PC è fuori dal suo valore di esistenza.
* Hint: Per debug è bene avere:
  * Una lista in modalità codice sorgente (HLL)
  * Una lista in modalità codice assembler (MIX)
  * In questo modo si vede meglio come le ottimizzazioni vanno a riordinare il codice di esecuzione, ed è più facile seguire il comportamento del codice.
  * NOTA IMPORTANTE: Guardare l'indicazione della vista in basso a destra, se quella indica che la vista è HLL ogni step che si farà sarà a livello di codice sorgente, anche se sto utilizzando la vista MIX!!!
* Beakpoint:
  * Per analizzare meglio i breakpoint si può fare riferimento alla guida `training_debugger.pdf`
  * I break point sono:
    * SW: In questo caso si crea una `trap`, si sostituisce il codice originale, che va a creare un eccezione e si può fermare il PC.
    * ONCHIP: Questo è un tipo diverso di break che sfrutta i comparatori presenti in HW, quindi il numero massimo è limitato.
      * Comodo se il codice rischia di essere sovrascritto o se bisogna inserire il break in un indirizzo virtuale, in questo caso infatti il breakpoint verrebbe neutralizzato
      * ON READ/WRITE: Scatta se avviene una scrittura o lettura, comodo ad es. se un registro viene modificato da una fonte non nota.
        * Per fare una scosa del genere su Linux sarebbe necessario mettere il breakpoint sull'indirizzo virtuale, ovvero quello usato da Linux.
  * È possibile creare anche dei breakpoint condizionati che scattano solo sotto certe condizioni impostabili.
    * In questo caso il sistema è intrusivo e questo si può capire vedendo l'indicazione di una `S` (rossa) in basso a destra.
  * Nella vista breakpoint questi sono indicati come: `<functname>\<offset dalla riga 0>`
* Tasto toolbar `i`:
  * Mostra tutte e variabili con il tipo di dato
  * Anche qui possono essere inseriti dei breakpoint per bloccarsi su una specifica var.
* Multicore debug SMT
  * È possibile accendere i core secondari sfruttando script lautherbach
  * Usando il comando `CORE.list` si può avere una lista dei core secondari da cui è possibile cambiare il contesto e lavorare con il core desiderato.
  * È possibile analizzare anche tutti i core assieme
  * Una volta impostato un breakpoint per un core, quello scatta per ogni core.
