# LAUTERBACH

## RTOS

* Boot di primo livello
  * Necessario avere i file elf
  * Conoscere gli indirizzi di memoria dove allocarli
    * Tramite script lauterbach è possibile trovare dove u-boot si va a rilocare nelle fasi di boot
  * Comando utile: `SYMbol.sourcepath.translate "<indirizzo di compilazione>" "<indirizzo dove sono allocati i sorgenti>"`
* Gestione della seriale di debug
  * I comandi ricadono sotto la famiglia `TERM`
  * È possibile da script impostare la periferica e la configurazione da utilizzare
  * Una volta configurato è possibile inviare comandi al DUT, il tutto sempre da script
* Verifica contenuto memoria.
  * È possibile fare un confronto tra un binario e ciò che è in memoria `Data.LOAD.Binary "" /DIFF` (il flag fa solo un confronto senza caricarlo)
    * Se il binario è diverso è possibile andare sostituirlo.
* Una volta avviato Linux verranno utilizzati indirizzi logici, per questo è necessario ricaricare il file elf all'indirizzo corretto. Per fare questo bisogna ricaricare l'elf con un offset del tipo `indirizzo logico + indirizzo fisico`
* Recap boot tramite T32:
  * Aprire la coneessione con il terminale di debug
  * Avviare un singolo core e bloccare l'avvio su u-boot
  * Eseguire il kernel linux allocando il file elf al giusto indirizzo di memoria
    * Questo richiede una riallocazione basata su indirizzamento logico e reale
  * Avviare i core secondari
  * Riagancciare il T32 a tutti i core
  * Completare il boot
* A questo punto è possibile debuggare tutto il sistema.
  * Si ha completo accesso al sistema Linux
  * Si possono vedere i processi eseguiti
    * Avendo l'elf dei programmi eseguiti in linux si potrebbero caricare anche quelli e debuggare le applicazioni in esecuzione
  * Si può analizzare il device-tree decodificato!!
  * Si possono analizzare i moduli, analizzando anche tutti processi che chiama e utilizza
* Post mortem analisys
  * in `linux/demo/arm/etc/coredump` è presente un esempio da vedere su simulatore per generare un core dump
  * Una possibile analisi post mortem si può fare tramite il Trace32 il quale può mostrare il log di quello che accaduto dal dump fino all'eccezione che ha portato a quel evento.
  * La stessa cosa si può ottenere anche verificando `Frame`, il quale mostra il log con meno informazione. È possibile mettere anche dei breakpoint sul coredump di sicurezza
  * Ci sono situazioni in cui la CPU non risponde neanche al break: Caso drammatico in cui i bus interni sono bloccati e la CPU non riesce a fare neanche un fetch.
    * In questo caso non si hanno molte informazioni, sarebbe necessario capire quale elf ha creato questo evento. Ma il debugger non fornisce informazioni neanche sul tipo di memoria associata al problema.
    * In questi casi bisogna andare un po' per tentativi. Si può tentare di fare un caricamento dell'elf che si sospetta abbia creato il problema.
    * In questo caso va caricato in una memoria di appoggio (virtual memory) che poi il sistema cerca di riallocare correttamente.
    * Da questa situazione si apre il codice caricato, che a questo punto va studiato per verificare se può effettivamente aver creato un problema.
  * Se non si ha a disposizione il TraceOnChip si può usare lo SNOOPEr
