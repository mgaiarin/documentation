# Flusso di lavoro progettazione HW
Cose da fare:
1. Per prima cosa creare una pagina wiki (c'è un template da cui partire).
2. La pagina deve mostrare info generali (PM, sviluppatori, ecc).
3. Repository/locazione repo progetto in R.
4. Requisiti HW -> tabella con requisito e descrizione (le specifiche sono interne o scelte con il cliente).
5. Per dettagliare alcuni passaggi si possono fare nuove pagine linkate.

## Altium designer:
AD è liberamente scaricabile dal sito, in DAVE è stato fatto un abbonamento annuale e gli aggioramenti sono forniti automaticamente.
Per questo non si trova una copia in R, risulta inutile avere versioni obsolete.
La licenza è documentata in wiki e ci sono 4 passaggi per registrare un nuovo utente.
1. Con l'account aziendale si possono creare nuovi utenti.
2. Poi l'utente può usare la licenza.
3. La licenza può essere usata da una persona alla volta.
4. Esistono anche gli spazi di lavoro in cui bisogna essere aggiunti per vedere i nostri progetti
5. Workspace DAVE
   1. Spazio di lavoro della DAVE con tutti i progetti
   2. Si possono aggiungere anche collaboratori esterni
   3. All'interno ci sono video introduttivi sullo spazio
   4. L'area di lavoro usa in background git -> quindi c'è un controllo di versione
      1. Flusso di lavoro su AD
         1. Se si fa il clone del repo e poi si fa il push si lascia in dietro il disco R
         2. Bisogna prestare attenzione per non lasciare indietro il disco R
         3. Sconsigliato fare modifiche in locale, meglio fare le modifiche direttamente dall'are di lavoro
6. Ci sono varie librerie in AD:
   1. Database library: Si utilizza questa in DAVE perché già strutturata come sul nuovo flusso di lavoro
      1. Libreria basata su Adempiere
         1. Spazio costantemente aggiornato
         2. I clienti non possono accedere ed inserire nuovi componenti
   2. Librerie spazio 365
      1. Hanno il vantaggio di condividere sia i progetti PCB sia le librerie
   3. Librerie spazio 365 PRO
   4. DataBAse library:
      1. Contenitore dove collegare tutti le librerie (Anche quelle esterne)
      2. Il DB Adempiere non è collegabile direttamente, ma si può esportare un excel (abbozzato tool automatico)
      3. Questa viene presa e va a selezionare i simboli di schematico e PCB
7. Ci sono tre template per pre impostare il lavoro:
   1. Template per schematico
   2. Template per PCB (TBD)
   3. Template file di output (Non presente in 365): Molto utile per preparare i gerber



