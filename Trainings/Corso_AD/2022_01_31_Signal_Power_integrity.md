# Signal and Power integrity
Corso base per avere una overview su questi apsetti. Questo tipo di problematiche va sempre preso in considerazione, si può andare un po' più tranquilli se il design è molto semplice e con pochi componenti. Quando si parla di "Signal Integrity" si parla di come un segnale si muove tra due punti, se l'informazione arriva corretta il segnale è intero. Quando si parla di questi argomenti si dovrebbe parlare di campi elettromagnetici e non del modello elettrone lacuna perchè questo modello tende a portare in inganno perchè considera finestre temporali troppo lunghe e non danno un modello accurato di quello che accade. Su segnali che commutano molto è come se gli elettroni non si muovessero molto "oscillano intorno ad un punto". Ciò che si valuta nella "Signal Integrity" dev'essere la geometria delle piste (lunghezza,spessore, ecc) su cui ho i segnali e cui si formano questi campi magnetici.  
Vediamo un esempio analizzando due diversi tipi di traccia:
1. Microstrip: Composto da una traccia che poggia sul dielettrico, e con un piano di riferimento
2. Striptline: Composto da una traccia annegata sul dielettrico e con due piano di riferimento (sopra e sotto)

Questa differenza costruttiva porta ad avere segnali elettrici con velocità diverse, questo perchè il campo magnetico è diverso essendo il dielettrico attorno alla pista diverso nei due casi -> Questo per far capire meglio che il modello elettrone lacuna ha poco senso in questi casi.

## Impedenza controllata
Quando valutare l'impedenza controllata?
1. Se distanza andata e ritorno pista minore del rise time si ha una traccia è non serve fare verifiche (non capita quasi mai)
2. Altrimenti è una traccia lunga è bisogna tenre conto dell'impedenza

### Percorso di ritorno
1. Va distinto dal piano di massa, perchè ci possono essere piani di ritorno diversi da quello di massa.

Si possono usare piani distinti come return path sfruttando condensatori di bypass o per accoppiamento diretto che ci danno il segnale di ritorno.

### Impedenza differenziale
In una coppia differenziale su PCB non si ha un accoppiamento perfetto come nei cavi, quindi bisogna considerare anche la single ended. 
Ad esempio se vogliamo 1000Ohm di differenziale bisgona avere 500Ohm nel sigle ended, in questo modo si preserva il segnale differenziale anche se l'accoppiamento non è perfetto.

### Switching vias
Vias aggiuntivi che servono a creare dei return path