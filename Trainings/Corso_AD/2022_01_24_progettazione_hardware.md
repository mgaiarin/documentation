# Progettazione HW
Le procedure seguite per iniziare un nuovo progetto si possono trovare nella pagina [wiki](http://davewiki.lan.dave.eu/wiki/index.php/Procedure.Sviluppo.Main).
Per quanto riguarda la parte HW le pagine più imoprtanti sono:
1. [Template:Documentazione_progetto](http://davewiki.lan.dave.eu/wiki/index.php/Procedure.Sviluppo.Main)
2. [Template:CheckList_Tecnica](http://davewiki.lan.dave.eu/wiki/index.php/Template:CheckList_Tecnica): Una guida che permette di aiutare durante lo svilluppo hw a non dimenticare nessun passaggio critico. Se durante un nuovo sviluppo si notano dei possibili nuovi punti si possono aggiungere. **Nota** Non è uno sviluppo passo-passo ma è una guida più generale. Tra le varie cose si possono trovare:
   1. Preferenze sui componenti da prendere.
   2. Tutto ciò che è da mettere sullo schema elettrico (es. componenti per schema elettrico).
   3. Nello schema elettrico:
      1. Usare schemi a blocchi / tabelle per aiutare a capire lo schematico.
      2. Usare molte note specificando tutte le problematiche affrontate / informazioni utili.
      3. Come preparare una BOM.
   4. Codifica di nuovi componenti (Si fa tramite Adempiere):
      1. Accedere ad adempiere e aggiungere un nuovo componente.
      2. Selezionare il tipo di componente per ottenere un nuovo codice identificativo Dave.
      3. Inserimento di dati ulteriori e codice produttore, le cui linee guida sono sulla pagina wiki.
         1. Per gestire componenti generici prodotti da vari produttori basta definire come produttore: VARI e inserire un codice produttore generico.
         2. Per gestire componenti specifici inserire il produttore e il codice specifico.
         3. Importante!! In ogni caso bisogna marcare il componente come: **Materia Prima**
         4. Quando vengono inserite tutte le informazioni si aggiunge il componente su Adempiere.
      4. Da adempiere si esporta il DB che poi verrà importato in Altium e che da informazioni sul componente da prendere dalla libreria dello schematico (presente in R:).
   5. Ci sono tre maggiori punti per efettuare il design di una scheda:
      1. Design For Testability (DFT): La scheda deve poter essere testata dal produttore per verificare la costruzione.
      2. Design For Manufactoring (DFM): La scheda deve essere creata e montata in maniera fattibile.
      3. Design For Debug (DFB): La scheda deve poter essere debuggata nelle fasi successive in maniera semplice.
   6. Bill of Materials (BOM)