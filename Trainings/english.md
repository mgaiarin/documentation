# English

* someone you can rely on
* I care about
* To share is to care
* S.0.P: handbook called standard operative procedure

## AT ON IN

* AT: specific time
  * e.g. I get up at 7'o'clock
* ON:
  * specific day
    * e.g. Monday
    * Don't say "on this Sunday"
    * never use the article "on the"!!
  * can be use o specific dated
    * e.g. on March 27th / on Christmas Day
    * use ordinal number for the day!!
* IN
  * used wit month
    * e.g. My birthday is in March
  * + year
    * e.g. The Titanic sank in 1912
  * Decade: use it with preposition "the"
    * e.g. in the sixties /in the 1960s
  * Century: use it with preposition "the"
    * e.g. It was built in the 16th century
  * season
    * e.g. We go to the beach in summer
  * period of time
    * it refer to a point of the time in the future
    * e.g. The meeting starts in ten minutes
* various
  * in the morning / afternoon
  * at night
  * on Wednesday afternoon and not on Wednesday in the afternoon
  * on the weekend / at the weekend
    * same meening the first one is US the second UK
  * At the moment = now, right now
    * in this moment NO!!!!
  * Last / next / this / every -> never use AT / ON / IN
    * e.g. The party is next Friday

## Future

* I can use presenti simple for the future. Only for something that will happen for sure!
  * I meet you tommorrow at DAVE
