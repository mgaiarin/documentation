# Day 4

## Analog Devices

Prodotti per alimentazioni

* uModule
  * Moduli di potenza completamente integrati
* Silent Switcher
  * DC/DC con prestazioni simili a LDO ma corrrenti superiori
* P90
  * Componenti a 90nm di nuova gnerazione
* Ultra low noise LDO
* Closed loop power
* nano power

### Power for FPGA

* Componenti che richiedono correnti sempre più alte (fino centinaia di ampere)
* Richiedono transitori sempre più veloci
* DC/DC
  * Compensati internamente
  * possono essere compensai anche con circuito esterno che permettono di filtrare il rumore in alta frequenza e avere una banda sufficientemente alta.
* Si cercano funzioni sempre più avanzate anche per i DC/DC
  * Monitoraggio e log in caso di fault
  * Gestione in caso fault tramite warning o stato di safety
* PMBUS
  * I2C con lane dedicate per il power managment
    * Ci sono molti componenti legacybasati su questo bus
    * Tra questi componenti di Analog e Maxim (acquisita)
  * Ci sono diversi tipi di componenti che gestiscono questo bus
    * componenti PMB puri
    * Componeti PMB con DCDC
    * Componenti totalmente integrati con PMB, DCDC e mos integrati
* High Precision Supervisor
  * Dispositivi con PMBus che monitora tensione con tolleranze sempre più strette
  * Ci sono versioni anche senza PMB

## Microchip

* Power estimation tool
  * Dopo aver fatto il place and root è possibile fare una stima dei consumi dell'FPGA

### Applicazioni PolarFire SoC e AMP

* Il sistema può essere diviso in una parte che fa girare Linux/Altro e una parte di core dedicati ad applicazioni real time
* Multi-hart
  * hart indica quello che è conosciuto come un core
  * Ci possoni essere sistemi omogenei (SMP) o eterogenei (AMP)
* Symmetric
  * Architettura omogenea con singolo kernel
  * Spazi di memoria condivisi
  * Task distribuiti tra i core
* Aymmetric
  * Sistema diviso in diversi contesti che si occupano di task diversi
  * Di default la memoria non è condivisa e va gestita l'allocazione delle risorse e del bus di counicazione
  * Si richiede un meccanismo di comunicazione tra sistemi
* RISC-V Core
  * Presente 1 core E51
    * Usato solitamente per il boot
    * Definito Monitor core
  * Presenti 4 Core U54
    * Definiti applicaton core
    * I core sono simili ma hanno istruction set diversi, U54 con set esteso
    * Presente anche la parte grafica in questi
* Cache
  * E1
    * Ha solo TCM e niente cache L1
  * U54
    * Aggiunta TCM L1
    * La cache viene in parte dedicata a memoria ad accesso diretto in cui inserire tutto ciò che è time critical
    * Questo lo rende deterministico, lavora come un microcontrollore
  * L1
    * Al boot le L1 sono conf come cache e poi possono essere dedicate come TCM
    * La configurazione può essere modificata in ogni momenti
  * L2
    * 2MB dedicati
    * Può essere usata come LIM. Stesso principio visto per la L1 ma con più memoria e per fare questo la L1 viene bypassata per garantire determinismo
      * Una volta definita come LIM non si può modificare
      * Se definita così finisce nel memory map del dispositivo
      * Questa memoria può essere condivisa tra tutti i core. Ovviamente la cosa va gestita.
    * C'è una modalità Scratchpad che lavora in modo misto in cui si usa LIM con uso della L1
    * Tutte le memorie sono coperte da ECC
* Presnti WDO che possono monitorare tutti i core
* Privileges mode
  * User mode
    * Minori privilegi
  * Spervisor mode
    * Privilegi di sistema con molti più privilegi ma non ha il completo controllo su tutto
  * Machine mode
    * Modalità di utilizzo con i privilegi massimi in cui si può ad accedere ad ogni registro
* Physical Memory Protection (PMP)
  * Gestione dell'allocazione delle risorse fisiche ai core
  * Questo sistema pemette di bloccare alcune periferiche che poi non potranno essere modificate neanche in machine mode
    * L'unico modo per sbloccare la periferica è richiesto un reset
  * Se qualcuno cerca di violare le specifiche definite è necessario eseguire un eccezzione
* Tool: MSS Configurator
  * Con questo tool è possibile configurare:
    * Periferiche
    * Allocazione delle cache con gestione LIM
    * PMP
      * Si consiglia di tenere E1 come monitor core di boot
      * Poi si consiglia di utilizzare 2 diversi contesti
        * Presenti due bus di comunicazione diversi
        * Se si aggiungono altri contesti bisogna considerare l'accesso ai bus condivisi
      * Si possono assegnare poi le periferiche ai vari contesti e queste configurazioni poi non possono essere più modificate, neanche in machine mode
* Comunicazione condivisa
  * Sfrutta RPMSg con il solito sistema code
  * La cosa in più è che a livello fisico è presente un sistema che genera interrupt e permette di gestire il determinismo il modo un po' migliore anche se poi a livello Linux non può essere garantito
  * Tutta la gestione fisica è presente già nei layer yocto di microchip

### Smart Embedded Vision

* Sistema intelligente sulla gestione video
* VectorBlox
  * Soluzione free per IP video
* VectorBox SDK
  * Forniscono networks pre allenate
  * Basato su C e pyhton
* Usandolo su PolarFire è possibile trovare un sistema già en integrato
  * Il core configura l'IP dell'FPGA ed è pronto per l'uso

## Lattice

* MachXO5-NX
  * Integrate le PSI flash, non nel silicio ma direttamente nel package
  * Tutta la piattaforma nexus è supportata dal tool Radiant e non Diamond

## Intel

* Prodotto Nios V basato su RISC-V
  * Si può usare qualsiasi idee che suppora RISC-V oppure usare IDE Intel
* Due tipologie di core
  * M
    * Supporta versione non PRO
  * G
    * Supportata solo nella versione PRO
    * Versione più evoluta con custom instruction e cache
  * Si prevedono altre varianti
* BSP
